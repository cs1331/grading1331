package grading1331;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.*;
import java.util.*;

import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.Map.entry;

import static org.junit.jupiter.api.Assertions.fail;

/**
 * A class with utilities related to constructors.
 * Namely, contains {@link #forceDefaultConstruction(Class)}, which is the primary way in which
 * instances of student classes are safely and securely obtained.
 * <br>
 * Additionally, contains some helper methods used to test constructor functionality like {@link #testConstructorFunctionality(Class, Class[], Object[], Map)}
 * @author Andrew Chafos
 */
public final class ConstructorHelper {

    public static final Map<Class<?>, Object> defaultValueMap
        = Map.ofEntries(
            entry(boolean.class, false), entry(Boolean.class, false), entry(byte.class, (byte) 0), entry(Byte.class, (byte) 0),
            entry(short.class, (short) 0), entry(Short.class, (short) 0), entry(int.class, 0), entry(Integer.class, 0),
            entry(long.class, 0L), entry(Long.class, 0L), entry(float.class, 0.0f), entry(Float.class, 0.0f),
            entry(double.class, 0.0), entry(Double.class, 0.0), entry(char.class, '\u0000'), entry(Character.class, '\u0000')
        );

    /**
     * Takes in a class and attempts to construct it using some available constructor.
     * This method should be used whenever an autograded test *ever* wants to get an instance of a class.
     * Note how we do not guarantee any fields being set here (we do not assume the student constructor works).
     * If you want to set field values, consider using one of the variations of {@link grading1331.ReflectHelper#getInstance}
     *<br>
     * When trying to construct a class, constructors are tried in order of the number of constructor parameters;
     * those with fewer parameters are tried first. The default primitive values are supplied for all primitives,
     * and this method is <b>recursively invoked</b> for all reference types.
     * <br>
     * If a constructor throws an exception, we move onto the next one.
     * <br>
     * All constructors are tried in 2 waves: in the first one, we try to make all the parameters non-null.
     * In the second one, if we encounter a parameter type that, when constructed, throws an exception for any reason,
     * we are allowed to make that parameter null. If this behavior is not desired, use {@link grading1331.ConstructorHelper#forceDefaultConstruction(Class, int, boolean)}
     * @param cls the class of which we wish to obtain an instance
     * @return the newly instantiated  object
     */
    public static Object forceDefaultConstruction(Class<?> cls) {
        PrintStream originalSysErr = System.err;
        try {
            System.setErr(new PrintStream(new ByteArrayOutputStream()));
            return forceDefaultConstruction(cls, 0, false);
        } finally {
            System.setErr(originalSysErr);
        }
    }

    public static Object forceDefaultConstruction(Class<?> cls, int defaultArraySize) {
        return forceDefaultConstruction(cls, defaultArraySize, true);
    }

    public static Object forceDefaultConstruction(Class<?> cls, int defaultArraySize, boolean forceNonNullParams) {
        return forceDefaultConstruction(cls, defaultArraySize, forceNonNullParams, Map.of());
    }

    /**
     * Takes in a class and attempts to construct it using some available constructor (see {@link grading1331.ConstructorHelper#forceDefaultConstruction(Class)})
     * When forceNonNullParams is set to true, no null parameter values are allowed. If this is not possible, the method
     * will fail
     * @param cls the class of which we wish to obtain an instance
     * @param forceNonNullParams when set to true, an object is only returned if it was created from a valid constructor
     *                           supplied strictly non-null parameters that did not throw an exception
     * @param methodFunctionMap mapping for abstract class abstract method implementation in the case an abstract class is constructed
     * @return the newly instantiated  object
     */
    public static Object forceDefaultConstruction(Class<?> cls, int defaultArraySize, boolean forceNonNullParams, Map<Method, Function<Object[], Object>> methodFunctionMap) {
        return forceDefaultConstruction(cls, defaultArraySize, forceNonNullParams, methodFunctionMap, new HashSet<>());
    }

    private static Object forceDefaultConstruction(Class<?> cls, int defaultArraySize, boolean forceNonNullParams,
                                                   Map<Method, Function<Object[], Object>> methodFunctionMap, HashSet<String> visitedSet) {
        if (defaultValueMap.containsKey(cls)) {
            return defaultValueMap.get(cls);
        } else if (cls.isEnum()) {
            if (forceNonNullParams && cls.getEnumConstants().length == 0) {
                fail("Could not get instance of non-null enum because there are 0 enum literals declared (problem constructing class " + cls.getSimpleName() + ")");
                return null;
            } else if (cls.getEnumConstants().length > 0) {
                return cls.getEnumConstants()[0];
            }
        } else if (cls.isArray()) {
            return Array.newInstance(cls.getComponentType(), defaultArraySize);
        } else if (cls.isInterface()) {
            Map<Method, Function<Object[], Object>> interfaceMethodFunctionMap = new HashMap<>();
            for (Method m: cls.getDeclaredMethods()) {
                interfaceMethodFunctionMap.put(m, (params) -> defaultValueMap.get(m.getReturnType()));
            }
            return ReflectHelper.getInterfaceInstance(cls, interfaceMethodFunctionMap);
        } else if (Modifier.isAbstract(cls.getModifiers())) {
            // TODO fix this for abstract classes that dont have only public constructors
            for (Constructor<?> toTry: cls.getDeclaredConstructors()) {
                try {
                    toTry.setAccessible(true);
                    HashSet<String> copy = new HashSet<>(visitedSet);
                    copy.add(cls.getSimpleName());
                    Object[] params = Arrays.stream(toTry.getParameterTypes()).map(paramClass -> forceDefaultConstruction(paramClass, defaultArraySize, forceNonNullParams, methodFunctionMap, copy)).toArray();
                    return ReflectHelper.getAbstractClassInstance(cls, methodFunctionMap, toTry.getParameterTypes(), params);
                } catch (Throwable ignored) {
                } finally {
                    if (!Modifier.isPublic(toTry.getModifiers())) {
                        toTry.setAccessible(false);
                    }
                }
            }
            fail("Could not use any constructors in the abstract class " + cls.getSimpleName() + " in order to create a concrete instance of it");
            return null;
        } else {
            Constructor<?>[] constructorArr = cls.getDeclaredConstructors();
            if (constructorArr.length == 0) {
                fail("Could not find a constructor from which we can create an instance of class " + cls.getSimpleName());
                return null;
            }
            List<Constructor<?>> sortedConstructors =
                Arrays.stream(cls.getDeclaredConstructors()).sorted(Comparator.comparingInt(Constructor::getParameterCount)).collect(Collectors.toList());
            for (int j = 1; j >= 0; j--) {
                boolean tryingAllNonNull = j == 1;
                for (Constructor<?> cons: sortedConstructors) {
                    Class<?>[] paramTypes = cons.getParameterTypes();
                    Object[] params = new Object[paramTypes.length];
                    boolean allParamsWorking = true;
                    for (int i = 0;i < paramTypes.length; i++) {
                        Class<?> paramType = paramTypes[i];
                        if (visitedSet.contains(paramType.getSimpleName())) {
                            if (forceNonNullParams) {
                                fail("Found a problematic circular relationship when attempting to construct class " + cls.getSimpleName());
                                return null;
                            }
                        } else {
                            HashSet<String> copy = new HashSet<>(visitedSet);
                            copy.add(cls.getSimpleName());
                            try {
                                params[i] = defaultValueMap.getOrDefault(paramType, forceDefaultConstruction(paramType, defaultArraySize, forceNonNullParams, methodFunctionMap, copy));
                            } catch (Throwable t) {
                                allParamsWorking = false;
                            }
                        }
                    }
                    if (allParamsWorking || (!tryingAllNonNull && !forceNonNullParams)) {
                        cons.setAccessible(true);
                        try {
                            return cons.newInstance(params);
                        } catch (InstantiationException | IllegalAccessException | InvocationTargetException e) {
                            e.printStackTrace();
                        } catch (Throwable ignored) {
                        }
                    }
                }
            }
        }
        fail(
            "Could not create an instance of class "
            + cls.getSimpleName() +
            " without every constructor throwing an exception, a circular relationship occurring, or an invalid enum being supplied when it needs to be non-null"
        );
        return null;
    }

    /**
     * Helper method to test to see if a particular constructor of a class works as expected
     * @param cls the class in which the constructor is expected to be declared
     * @param paramTypes the expected parameter types of the constructor to be tested
     * @param params the parameters that are supplied to the constructor for this test case
     * @param fieldsToCheck an array of Strings representing the fields whose values we wish to check
     * @param expectedFieldValues the corresponding expected values for the fields with names specified by fieldsToCheck, in order
     */
    public static void testConstructorFunctionality(Class<?> cls, Class<?>[] paramTypes, Object[] params,
                                                    String[] fieldsToCheck, Object[] expectedFieldValues) {
        Constructor<?> cons = ReflectHelper.getDeclaredConstructor(cls, paramTypes);
        Object instance = ReflectHelper.getInstance(cons, params);
        if (fieldsToCheck.length != expectedFieldValues.length) {
            throw new IllegalArgumentException("Number of fields and number of corresponding values to set must be equal!");
        }
        for (int i = 0;i < fieldsToCheck.length; i++) {
            ReflectAssert.assertSameSafely(expectedFieldValues[i], ReflectHelper.getFieldValue(fieldsToCheck[i], cls, instance));
        }
    }

    /**
     * Helper method to test to see if a particular constructor of a class works as expected
     * <br>
     * The difference between this method and {@link grading1331.ConstructorHelper#testConstructorFunctionality(Class, Class[], Object[], String[], Object[])}
     * is that, instead of the parameter types needing to be explicitly supplied, we simply let the parameter type be the Class<?>
     * object of the corresponding parameter when it is non-null and Object.class when it is null.
     * @param cls the class in which the constructor is expected to be declared
     * @param params the parameters that are supplied to the constructor for this test case
     * @param fieldsToCheck an array of Strings representing the fields whose values we wish to check
     * @param expectedFieldValues the corresponding expected values for the fields with names specified by fieldsToCheck, in order
     */
    public static void testConstructorFunctionality(Class<?> cls, Object[] params,
                                                    String[] fieldsToCheck, Object[] expectedFieldValues) {
        testConstructorFunctionality(cls, ReflectHelper.getParamTypesFromParams(params), params, fieldsToCheck, expectedFieldValues);
    }

    /**
     * Helper method to test to see if a particular constructor of a class works as expected
     * @param cls the class in which the constructor is expected to be declared
     * @param paramTypes the expected parameter types of the constructor to be tested
     * @param params the parameters that are supplied to the constructor for this test case
     * @param expectedValueMap a mapping from the names of fields to check to the Objects they are supposed to be
     *                         after the constructor is invoked
     */
    public static void testConstructorFunctionality(Class<?> cls, Class<?>[] paramTypes, Object[] params,
                                                    Map<String, Object> expectedValueMap) {
        Constructor<?> cons = ReflectHelper.getDeclaredConstructor(cls, paramTypes);
        Object instance = ReflectHelper.getInstance(cons, params);
        for (String fieldName: expectedValueMap.keySet()) {
            ReflectAssert.assertSameSafely(expectedValueMap.get(fieldName), ReflectHelper.getFieldValue(fieldName, cls, instance));
        }
    }

    /**
     * Helper method to test to see if a particular constructor of a class works as expected
     * <br>
     * The difference between this method and {@link grading1331.ConstructorHelper#testConstructorFunctionality(Class, Class[], Object[], Map)}
     * is that, instead of the parameter types needing to be explicitly supplied, we simply let the parameter type be the Class<?>
     * object of the corresponding parameter when it is non-null and Object.class when it is null.
     * @param cls the class in which the constructor is expected to be declared
     * @param params the parameters that are supplied to the constructor for this test case
     * @param expectedValueMap a mapping from the names of fields to check to the Objects they are supposed to be
     *                         after the constructor is invoked
     */
    public static void testConstructorFunctionality(Class<?> cls, Object[] params, Map<String, Object> expectedValueMap) {
        testConstructorFunctionality(cls, ReflectHelper.getParamTypesFromParams(params), params, expectedValueMap);
    }

}
