package grading1331;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.tree.AbstractInsnNode;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.MethodInsnNode;
import org.objectweb.asm.tree.MethodNode;

import java.io.File;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.stream.Collectors.joining;
import static org.junit.jupiter.api.Assertions.*;

/**
 * Assertion utilities for Java reflection.
 *
 * @author jdierberger3f, Shishir, achafos3, and nzhong31
 * @version 1.0
 */
public final class ReflectAssert {

    /**
     * Constructor used to prevent Instances of ReflectAssert from being created
     */
    private ReflectAssert() { }

    private static final int NOT_PACKAGE_PROTECTED = Modifier.PUBLIC | Modifier.PROTECTED | Modifier.PRIVATE;

    private static boolean isPackageProtected(int mod) {
        return (mod & NOT_PACKAGE_PROTECTED) == 0;
    }

    /**
     * Assert the given Member is private.
     *
     * @param m The Member to check.
     */
    public static void assertPrivate(Member m) {
        assertModifier(m, "private", Modifier::isPrivate);
    }

    /**
     * Assert the given Member is protected.
     *
     * @param m The Member to check.
     */
    public static void assertProtected(Member m) {
        assertModifier(m, "protected", Modifier::isProtected);
    }

    /**
     * Assert the given Member is public.
     *
     * @param m The Member to check.
     */
    public static void assertPublic(Member m) {
        assertModifier(m, "public", Modifier::isPublic);
    }

    /**
     * Assert the given Member is package-protected
     *
     * @param m The Member to check.
     */
    public static void assertPackageProtected(Member m) {
        assertModifier(m, "package-protected", ReflectAssert::isPackageProtected);
    }

    /**
     * Assert the given Member is static.
     *
     * @param m The Member to check.
     */
    public static void assertStatic(Member m) {
        assertModifier(m, "static", Modifier::isStatic);
    }

    /**
     * Assert the given Member is not static
     *
     * @param m the Member to check
     */
    public static void assertNotStatic(Member m) {
        assertFalse(
                Modifier.isStatic(m.getModifiers()),
                getMemberDescriptiveName(m) + " in class "
                        + m.getDeclaringClass().getSimpleName() + " must be non-static"
        );
    }

    /**
     * Assert the given Class is not static
     *
     * @param c the Class to check
     */
    public static void assertNotStatic(Class<?> c) {
        assertFalse(Modifier.isStatic(c.getModifiers()), "Class " + c.getSimpleName() + " must be non-static");
    }

    /**
     * Assert the given Member is final.
     *
     * @param m The Member to check.
     */
    public static void assertFinal(Member m) {
        assertModifier(m, "final", Modifier::isFinal);
    }

    /**
     * Assert the given Member is not final.
     *
     * @param m The Member to check.
     */
    public static void assertNotFinal(Member m) {
        assertFalse(Modifier.isFinal(m.getModifiers()), "Member " + m.getName() + " must be non-final");
    }

    /**
     * Assert the given Member is abstract.
     *
     * @param m The Member to check.
     */
    public static void assertAbstract(Member m) {
        assertModifier(m, "abstract", Modifier::isAbstract);
    }

    /**
     * Assert the given Member is not abstract.
     *
     * @param m The Member to check.
     */
    public static void assertNotAbstract(Member m) {
        assertFalse(Modifier.isAbstract(m.getModifiers()), "Member " + m.getName() + " must be non-abstract");
    }

    /**
     * Assert the given Member is an interface.
     *
     * @param m The Member to check.
     */
    public static void assertInterface(Member m) {
        assertModifier(m, "interface", Modifier::isInterface);
    }

    /**
     * Assert the given Class is private.
     *
     * @param c The Class to check.
     */
    public static void assertPrivate(Class<?> c) {
        assertModifier(c, "private", Modifier::isPrivate);
    }

    /**
     * Assert the given Class is protected.
     *
     * @param c The Class to check.
     */
    public static void assertProtected(Class<?> c) {
        assertModifier(c, "protected", Modifier::isProtected);
    }

    /**
     * Assert the given Class is public.
     *
     * @param c The Class to check.
     */
    public static void assertPublic(Class<?> c) {
        assertModifier(c, "public", Modifier::isPublic);
    }

    /**
     * Assert the given Class is package-protected.
     *
     * @param c The Class to check.
     */
    public static void assertPackageProtected(Class<?> c) {
        assertModifier(c, "package-protected", ReflectAssert::isPackageProtected);
    }

    /**
     * Assert the given Class is static.
     *
     * @param c The Class to check.
     */
    public static void assertStatic(Class<?> c) {
        assertModifier(c, "static", Modifier::isStatic);
    }

    /**
     * Assert the given Class is final.
     *
     * @param c The Class to check.
     */
    public static void assertFinal(Class<?> c) {
        assertModifier(c, "final", Modifier::isFinal);
    }

    /**
     * Assert the given Class is not final.
     *
     * @param c The Class to check.
     */
    public static void assertNotFinal(Class<?> c) {
        assertModifier(c, "final", Modifier::isFinal);
    }

    /**
     * Assert the given Class is abstract.
     *
     * @param c The Class to check.
     */
    public static void assertAbstract(Class<?> c) {
        assertModifier(c, "abstract", Modifier::isAbstract);
    }

    /**
     * Assert the given Class is an interface.
     *
     * @param c The Class to check.
     */
    public static void assertInterface(Class<?> c) {
        assertModifier(c, "interface", Modifier::isInterface);
    }

    private static void assertModifier(
            String message, Member member, String check, Function<Integer, Boolean> checker) {
        assertTrue(
            checker.apply(member.getModifiers()),
            message + ". " + getMemberDescriptiveName(member) + " in class "
                + member.getDeclaringClass().getSimpleName() + " must be " + check
        );
    }

    private static void assertModifier(Member member, String check, Function<Integer, Boolean> checker) {
        assertTrue(
            checker.apply(member.getModifiers()),
            getMemberDescriptiveName(member) + " in class " + member.getDeclaringClass().getSimpleName()
                + " must be " + check
        );
    }

    private static void assertModifier(String message, Class clazz, String check, Function<Integer, Boolean> checker) {
        assertTrue(
            checker.apply(clazz.getModifiers()),
            message + ". " + clazz.getSimpleName() + " must be " + check
        );
    }

    private static void assertModifier(Class clazz, String check, Function<Integer, Boolean> checker) {
        assertTrue(
            checker.apply(clazz.getModifiers()),
            clazz.getSimpleName() + " must be " + check
        );
    }

    private static String getMemberDescriptiveName(Member member) {
        if (member instanceof Constructor) {
            var cons = (Constructor<?>) member;
            var parameters = Arrays.stream(cons.getGenericParameterTypes())
                                .map(Object::toString).collect(joining(", "));
            return "Constructor " + cons.getName() + "(" + parameters + ")";
        } else if (member instanceof Method) {
            var meth = (Method) member;
            var parameters = Arrays.stream(meth.getGenericParameterTypes())
                                .map(Object::toString).collect(joining(", "));
            return "Method " + meth.getName() + "(" + parameters + ")";
        } else if (member instanceof Field) {
            var field = (Field) member;
            return "Field " + field.getName();
        } else {
            return member.getName();
        }
    }

    /**
     * Assert the given Member is private.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertPrivate(String message, Member m) {
        assertModifier(message, m, "private", Modifier::isPrivate);
    }

    /**
     * Assert the given Member is protected.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertProtected(String message, Member m) {
        assertModifier(message, m, "protected", Modifier::isProtected);
    }

    /**
     * Assert the given Member is public.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertPublic(String message, Member m) {
        assertModifier(message, m, "public", Modifier::isPublic);
    }

    /**
     * Assert the given Member is package-protected.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertPackageProtected(String message, Member m) {
        assertModifier(message, m, "package-protected", ReflectAssert::isPackageProtected);
    }

    /**
     * Assert the given Member is static.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertStatic(String message, Member m) {
        assertModifier(message, m, "static", Modifier::isStatic);
    }

    /**
     * Assert the given Member is final.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertFinal(String message, Member m) {
        assertModifier(message, m, "final", Modifier::isFinal);
    }

    /**
     * Assert the given Member is abstract.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertAbstract(String message, Member m) {
        assertModifier(message, m, "abstract", Modifier::isAbstract);
    }

    /**
     * Assert the given Member is an interface.
     *
     * @param message The fail message.
     * @param m The Member to check.
     */
    public static void assertInterface(String message, Member m) {
        assertModifier(message, m, "interface", Modifier::isInterface);
    }

    /**
     * Assert the given Class is private.
     *
     * @param message The fail message.
     * @param c The Class to check.
     */
    public static void assertPrivate(String message, Class<?> c) {
        assertModifier(message, c, "private", Modifier::isPrivate);
    }

    /**
     * Assert the given Class is protected.
     *
     * @param message The fail message.
     * @param c The Class to check.
     */
    public static void assertProtected(String message, Class<?> c) {
        assertModifier(message, c, "protected", Modifier::isProtected);
    }

    /**
     * Assert the given Class is public.
     *
     * @param message  The fail message.
     * @param c The Class to check.
     */
    public static void assertPublic(String message, Class<?> c) {
        assertModifier(message, c, "public", Modifier::isPublic);
    }

    /**
     * Assert the given Class is package-protected.
     *
     * @param message  The fail message.
     * @param c The Class to check.
     */
    public static void assertPackageProtected(String message, Class<?> c) {
        assertModifier(message, c, "package-protected", ReflectAssert::isPackageProtected);
    }

    /**
     * Assert the given Class is static.
     *
     * @param message The fail message.
     * @param c The Class to check.
     */
    public static void assertStatic(String message, Class<?> c) {
        assertModifier(message, c, "static", Modifier::isStatic);
    }

    /**
     * Assert the given Class is final.
     *
     * @param message The fail message.
     * @param c The Class to check.
     */
    public static void assertFinal(String message, Class<?> c) {
        assertModifier(message, c, "final", Modifier::isFinal);
    }

    /**
     * Assert the given Class is abstract.
     *
     * @param message The fail message.
     * @param c The Class to check.
     */
    public static void assertAbstract(String message, Class<?> c) {
        assertModifier(message, c, "abstract", Modifier::isAbstract);
    }

    /**
     * Assert the given Class is an interface.
     *
     * @param message The fail message.
     * @param c The Class to check.
     */
    public static void assertInterface(String message, Class<?> c) {
        assertModifier(message, c, "interface", Modifier::isInterface);
    }

    /**
     * Assert the given Member has at least one of the modifiers listed.
     *
     * @param message The fail message.
     * @param m The Member to check.
     * @param modifiers The modifiers to check.
     */
    public static void assertOrModifiers(String message, Member m, int... modifiers) {
        for (int mod : modifiers) {
            if ((m.getModifiers() & mod) != 0) {
                return;
            }
        }
        fail(message);
    }

    /**
     * Assert the given Class has at least one of the modifiers listed.
     *
     * @param message The fail message.
     * @param c The Class to check.
     * @param modifiers The modifiers to check.
     */
    public static void assertOrModifiers(String message, Class<?> c, int... modifiers) {
        for (int mod : modifiers) {
            if ((c.getModifiers() & mod) != 0) {
                return;
            }
        }
        fail(message);
    }

    //

    /**
     * Assert the given Class has the given Field.
     * @param cls The Class to check.
     * @param name The Field name to check.
     */
    public static void assertFieldExists(Class<?> cls, String name) {
        try {
            cls.getDeclaredField(name);
        } catch (NoSuchFieldException nsfe) {
            fail("Field " + name + " does not exist in " + cls.getSimpleName());
        } catch (SecurityException se) {
            fail("SecurityExeption occured");
        }
    }

    /**
     * Assert the given Class has the given Field.
     * @param message The message to fail with.
     * @param cls The Class to check.
     * @param name The Field name to check.
     */
    public static void assertFieldExists(String message, Class<?> cls, String name) {
        try {
            cls.getDeclaredField(name);
        } catch (NoSuchFieldException nsfe) {
            fail(message);
        } catch (SecurityException se) {
            fail("SecurityExeption occured");
        }
    }

    /**
     * Assert the given Class has the given Method.
     * @param cls The Class to check.
     * @param name The Method name to check.
     * @param params The Method parameters to check.
     */
    public static void assertMethodExists(Class<?> cls, String name, Class<?>...params) {
        try {
            cls.getDeclaredMethod(name, params);
        } catch (NoSuchMethodException nsme) {
            fail("Method " + name + " with parameters " + Arrays.toString(params)
                + " does not exist in " + cls.getSimpleName());
        } catch (SecurityException se) {
            fail("SecurityExeption occured");
        }
    }

    /**
     * Assert the given Class has the given Method.
     * @param message The message to fail with.
     * @param cls The Class to check.
     * @param name The Method name to check.
     * @param params The Method parameters to check.
     */
    public static void assertMethodExists(String message, Class<?> cls, String name, Class<?>...params) {
        try {
            cls.getDeclaredMethod(name, params);
        } catch (NoSuchMethodException nsme) {
            fail(message);
        } catch (SecurityException se) {
            fail("SecurityExeption occured");
        }
    }

    /**
     * Assert the given Field has the given type.
     * @param field The Field to check.
     * @param type The type it should have.
     */
    public static void assertFieldType(Field field, Class<?> type) {
        assertEquals(type, field.getType(),
            "Field " + field.getName() + " has incorrect type: "
        );
    }

    /**
     * Assert the given Field has the given type.
     * @param message The message to fail with.
     * @param field The Field to check.
     * @param type The type it should have.
     */
    public static void assertFieldType(String message, Field field, Class<?> type) {
        assertEquals(type, field.getType(), message);
    }

    /**
     * Assert the given field has the given value in the given instance.
     * @param f The Field you're asserting over.
     * @param instance The instance to assert has the value.
     * @param expected The expected value of the field.
     */
    public static void assertFieldHasValue(Field f, Object instance, Object expected) {
        assertEquals(expected, ReflectHelper.getFieldValue(f, instance));
    }

    /**
     * Assert the given field has the given value in the given instance.
     * @param message The message to fail with.
     * @param f The Field you're asserting over.
     * @param instance The instance to assert has the value.
     * @param expected The expected value of the field.
     */
    public static void assertFieldHasValue(String message, Field f, Object instance, Object expected) {
        assertEquals(expected, ReflectHelper.getFieldValue(f, instance), message);
    }

    /**
     * Assert 2 Objects are exactly the same in memory.  Call assertSame if they are not
     * primitives, and call assertEquals if they are
     * @param instance1 first instance
     * @param instance2 other instance to be compared against the first
     */
    public static void assertSameSafely(Object instance1, Object instance2) {
        String message = String.format(
            "Expected values %s and %s to be the same, but they were different", instance1, instance2
        );
        if (instance1 == null) {
            assertNull(instance2, message);
        } else if (ReflectHelper.primitiveAndWrapperTypes.contains(instance1.getClass())){
            assertEquals(instance1, instance2);
        } else {
            assertSame(instance1, instance2);
        }
    }

    /**
     * Assert 2 Objects are exactly the same in memory.  Call assertSame if they are not
     * primitives, and call assertEquals if they are
     * @param instance1 first instance
     * @param instance2 other instance to be compared against the first
     * @param message message to display if the 2 Objects are not equal
     */
    public static void assertSameSafely(Object instance1, Object instance2, String message) {
        if (instance1 == null) {
            assertNull(instance2, message);
        } else if (ReflectHelper.primitiveAndWrapperTypes.contains(instance1.getClass())) {
            assertEquals(instance1, instance2, message);
        } else {
            assertSame(instance1, instance2, message);
        }
    }

    /**
     * Asserts that a method throws the expected exception.
     * @param mir Method invocation report from invoking the method
     * @param exceptionClass Class of the exception that should've been thrown
     */
    public static void assertExceptionThrown(ReflectHelper.MethodInvocationReport mir, Class<?> exceptionClass) {
        assertTrue(mir.exception.isPresent(), mir.methodName + " should have thrown an exception for this case!");
        assertEquals(exceptionClass, mir.exception.get().getClass(),
                mir.methodName + " should have thrown " + exceptionClass.getSimpleName());
    }

    /**
     * Asserts that a method does not throw an exception
     * @param mir Method invocation report from invoking the method
     */
    public static void assertExceptionNotThrown(ReflectHelper.MethodInvocationReport mir) {
        String exceptionString = mir.exception.isPresent() ? mir.exception.get().toString() : "";
        assertFalse(mir.exception.isPresent(),
                mir.methodName + " threw an exception when it shouldn't have: " + exceptionString);
    }

    /**
     * Asserts that a local variable within a method inside a class has the correct type
     * @param fileName name of the file where the method and variable is located
     * @param classObject object representing the class
     * @param methodName name of the method we are looking in
     * @param variableName name of the variable we are looking for
     * @param type expected type of the local variable
     */
    public static void assertLocalVariableType(String fileName, Class<?> classObject, String methodName,
                                               String variableName, Class<?> type) {
        Map<Class<?>, String> typeMap = new HashMap<>();
        typeMap.put(short.class, "S");
        typeMap.put(byte.class, "B");
        typeMap.put(int.class, "I");
        typeMap.put(long.class, "L");
        typeMap.put(double.class, "D");
        typeMap.put(float.class, "F");
        typeMap.put(char.class, "C");
        typeMap.put(boolean.class, "Z");
        typeMap.put(String.class, "Ljava/lang/String;");

        if (type.toString().contains("[")) {
            Pattern primitivePattern = Pattern.compile("\\[(.*)");
            Matcher matcher = primitivePattern.matcher(type.toString());
            assertTrue(matcher.find(), "Could not find array bracket in type");

            String array = matcher.group().replace('.', '/');
            assertEquals(array, ReflectHelper.getLocalVariableType(fileName, classObject, methodName,
                    variableName));
        } else if (!typeMap.containsKey(type)) {
            String typeConverted = "L" + type.getName().replace('.', '/') + ";";
            assertEquals(typeConverted, ReflectHelper.getLocalVariableType(fileName, classObject, methodName,
                    variableName));
        } else {
            assertEquals(typeMap.get(type), ReflectHelper.getLocalVariableType(fileName, classObject, methodName,
                    variableName));
        }
    }

    /**
     * Asserts that a method is called a certain amount of times within another method.
     * @param fileName name of the file where the method and variable is located
     * @param classObject object representing the class
     * @param methodName name of the method we are looking in
     * @param calledMethod method being called within methodName
     * @param count amount of method calls
     * @param flag -1 for <= count, 0 for = count, 1 for >= count
     */
    public static void assertMethodCallsWithinMethod(String fileName, Class<?> classObject, String methodName,
                                                     String calledMethod, int count, int flag) {
        try {
            fileName = fileName.replace('.', File.separatorChar) + ".class";
            ClassReader cr = new ClassReader(classObject.getClassLoader().getResourceAsStream(fileName));
            ClassNode cn = new ClassNode();
            cr.accept(cn, 0);

            int cnt = 0;
            for (MethodNode method : (List<MethodNode>) cn.methods) {
                if (method.name.equals(methodName)) {
                    for (AbstractInsnNode node : method.instructions) {
                        if (node instanceof MethodInsnNode) {
                            if (((MethodInsnNode) node).name.contains(calledMethod)) {
                                cnt += 1;
                            }
                        }
                    }
                }
            }
            if (flag == -1) {
                assertTrue(cnt <= count, String.format("Expected %s to call %s at most %d times but found %d" +
                        " calls", methodName, calledMethod, count, cnt));
            } else if (flag == 0) {
                assertTrue(cnt == count, String.format("Expected %s to call %s %d times but found %d" +
                        " calls", methodName, calledMethod, count, cnt));
            } else if (flag == 1) {
                assertTrue(cnt >= count, String.format("Expected %s to call %s at least %d times but found %d" +
                        " calls", methodName, calledMethod, count, cnt));
            }
        } catch (Exception ignored) {}
    }
}
