package grading1331.gradescope;

/**
 * An enum used to denote the visibility level of tests.
 * <br>
 * When Visibility.VISIBLE is used, the test case will always be shown to students
 * <br>
 * When Visibility.HIDDEN is used, the test case will never be shown to students
 * <br>
 * When Visibility.AFTER_PUBLISHED is used, the test case will be shown only when
 * the assignment is explicitly published from the "Review Grades" page
 * <br>
 * When Visibility.AFTER_DUE_DATE is used, the test case will be shown after the assignment's
 * due date has passed. If late submissions are allowed, then the test will be shown only after the late due date.
 * <br>
 * More information can be found at
 * <a href="https://gradescope-autograders.readthedocs.io/en/latest/specs/#output-format">this web page</a>.
 * @author Andrew Chafos
 */
public enum Visibility {
    VISIBLE, HIDDEN, AFTER_PUBLISHED, AFTER_DUE_DATE
}
