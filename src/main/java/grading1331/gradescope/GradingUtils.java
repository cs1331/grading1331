package grading1331.gradescope;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.puppycrawl.tools.checkstyle.Main;

import grading1331.ReflectHelper;

import static org.junit.jupiter.api.Assertions.*;

import javax.tools.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.Permission;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * Contains a variety of methods relevant to running various tasks that are generally
 * outside of the scope of normal Autograder tests but are absolutely necessary for the current
 * autograder framework, including runTestSuiteWithArguments, which kicks off an entire autograder
 * test suite with complex parameters, as well as methods to setup and run Checkstyle, check for
 * illegal imports and usages, and compile Java files.
 * <br>
 * The intended use for runTestSuiteWithArguments is for it to be called in a class with a main method that
 * has access to some wider project with Test classes. In the current recommended setup, this wider project
 * uses the Gradle build tool.
 * @author Andrew Chafos
 */
public class GradingUtils {

    private static List<List<File>> allNonEmptySubsets(List<File> list) {
        if (list.size() == 1) {
            List<List<File>> toReturn = new ArrayList<>();
            toReturn.add(list);
            return toReturn;
        } else {
            List<List<File>> outerList = new ArrayList<>();
            for (File file: list) {
                ArrayList<File> subList = new ArrayList<>(list);
                subList.remove(file);
                outerList.addAll(allNonEmptySubsets(subList));
            }
            outerList.add(list);
            return outerList;
        }
    }

    /**
     * Compiles the given Java file with a list of provided dependency Java files,
     * and returns the Corresponding List of Compiler Errors.
     * If something goes wrong with the internal compiling mechanism used to generate the Compiler Error
     * list, then a List containing a String with the error will be returned.
     * Since JavaFX is a dependency of this project, it is included as a resource by default when compiling.
     * @param initFileList a List of student Files to be compiled
     * @return an empty list if the file compiles, or a list of nicely formatted Strings will be returned
     * that list the Compiler Errors
     */
    public static List<String> getCompilerErrors(List<File> initFileList) {
        try {
            List<String> toReturn = List.of();
            for (int i = initFileList.size(); i >= 1; i--) {
                final int size = i;
                List<List<File>> allSubsets =
                    allNonEmptySubsets(initFileList).stream().filter(list -> list.size() == size).collect(Collectors.toList());
                boolean done = false;
                for (List<File> fileList: allSubsets) {
                    DiagnosticCollector<JavaFileObject> diagnostics = new DiagnosticCollector<>();
                    JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
                    StandardJavaFileManager fileManager = compiler.getStandardFileManager(diagnostics, null, Charset.forName("UTF-8"));
                    File[] fileArr = new File[fileList.size()];
                    Iterable<? extends JavaFileObject> compilationUnit = fileManager.getJavaFileObjects(fileList.toArray(fileArr));
                    List<String> options = new ArrayList<>();
                    options.add("-g");
                    compiler.getTask(null, fileManager, diagnostics, options, null, compilationUnit).call();
                    fileManager.close();
                    List<String> compilerErrors = diagnostics.getDiagnostics().stream().map(
                        diagnostic -> String.format("%nCompile %s on %s:%d:%d - %s. %s",
                            diagnostic.getKind(),
                            new File(diagnostic.getSource().getName()).getName(), diagnostic.getLineNumber(),
                            diagnostic.getColumnNumber(), diagnostic.getMessage(Locale.ENGLISH), getExtraOutput(diagnostic)
                        )
                    ).collect(Collectors.toList());
                    if (compilerErrors.isEmpty()) {
                        done = true;
                    } else if (size == initFileList.size()) {
                        toReturn = compilerErrors;
                    }
                }
                if (done) {
                    break;
                }
            }
            return toReturn;
        } catch (Throwable t) {
            String standardError = String.format("Could not compile one or more input files due to %s", t.getMessage());
            return List.of(standardError);
        }
    }

    private static String getExtraOutput(Diagnostic<? extends JavaFileObject> diagnostic) {
        if (diagnostic.getKind() != Diagnostic.Kind.ERROR) {
            return "\n\nThis is not a compiler error, just a warning/suggestion."
            + " If you would still like to fix it, you can run javac with the extra recommended flag"
            + " the error message tells you about BEFORE the source file but after any other flags";
        }
        return "";
    }

    /**
     * Compiles a single java file.
     * If the file compiles, an empty list will be returned.
     * Otherwise, a list of nicely formatted Strings will be returned that list the Compiler Errors
     * If something goes wrong with the internal compiling mechanism used to generate the Compiler Error
     * list, then a List containing a String with the error will be returned
     * @param studentFile a File object with the student File to compile
     * @return an empty list if the file compiles, or a list of nicely formatted Strings will be returned
     * that list the Compiler Errors
     */
    public static List<String> getCompilerErrors(File studentFile) {
        return getCompilerErrors(List.of(studentFile));
    }

    /**
     * Determines whether a given Java file with a List of provided dependencies compiles
     * @param studentFile the Java file to compile
     * @param dependencies a List of Java dependencies with which studentFile should be compiled
     * @return true if studentFile compiles with the list of dependencies, false otherwise
     */
    public static boolean fileCompiles(File studentFile, List<File> dependencies) {
        List<File> combinedList = new ArrayList<>(dependencies);
        combinedList.add(studentFile);
        return getCompilerErrors(combinedList).isEmpty();
    }

    /**
     * Determines whether a given Java file compiles
     * @param studentFile the Java file to compile
     * @return true if studentFile compiles, false otherwise
     */
    public static boolean fileCompiles(File studentFile) {
        return fileCompiles(studentFile, new ArrayList<>());
    }

    /**
     * These classes are essentially only used to enable and disable System.exit, but they
     * could in theory be used for other things as well.
     */
    public static class ExitTrappedException extends SecurityException {
        public final int numErrors;
        public ExitTrappedException(int numErrors) {
            this.numErrors = numErrors;
        }
    }

    private static class StopExitSecurityManager extends SecurityManager {
        private SecurityManager prevMgr = System.getSecurityManager();
        public void checkPermission(Permission perm) {}

        public void checkExit(int status) {
            super.checkExit(status);
            throw new ExitTrappedException(status);
        }

        public SecurityManager getPreviousMgr() {
            return prevMgr;
        }
    }

    /**
     * Disables System.exit(int errorCode) from having any effect on program flow.
     * Instead, a Runtime ExitTrappedException will be thrown whenever System.exit is invoked.
     */
    public static void disableSystemExit() {
        SecurityManager securityManager = new StopExitSecurityManager();
        System.setSecurityManager(securityManager);
    }

    /**
     * Re-enables System.exit(int errorCode) in order to allow it to normally affect program flow again.
     */
    public static void enableSystemExit() {
        SecurityManager mgr = System.getSecurityManager();
        if ((mgr instanceof StopExitSecurityManager)) {
            StopExitSecurityManager smgr = (StopExitSecurityManager) mgr;
            System.setSecurityManager(smgr.getPreviousMgr());
        } else {
            System.setSecurityManager(null);
        }
    }

    /**
     * The File Names on which Checkstyle should be run
     */
    public static List<String> filesToCheck = new ArrayList<>();
    /**
     * When set to true, non-Javadoc Checkstyle tests will be run
     */
    public static boolean checkstyleEnabled = false;
    /**
     * When set to true, Javadoc tests will be run
     */
    public static boolean javadocEnabled = false;
    /**
     * the maximum number of permissible total Checkstyle + Javadoc deductions
     * Should be a positive integer
     */
    public static int checkstyleCap = 0;

    /**
     * The amount of points to deduct for an Illegal Import
     */
    public static int illegalImportDeduction = 0;
    /**
     * The amount of points to deduct for an Illegal Usage
     */
    public static int illegalUsageDeduction = 0;
    /**
     * a List of packages and classes the student is allowed to use for the current assignment. Based on Regex.
     */
    public static List<String> allowedImports = new ArrayList<>();
    /**
     * A list of general expressions/phrases that should not be allowed. Based on Regex.
     */
    public static List<String> illegalUsages = new ArrayList<>();

    /**
     * List of Checkstyle Errors which can be ignored.  Based on comparing the checkstyle output to the
     * Strings in this List directly, no Regex involved.
     */
    public static List<String> allowedCheckstyleErrors = new ArrayList<>();

    /**
     * When set to true, the overall Gradescope score will become 0.0 instead of a negative number in the case
     * that it is negative. Otherwise, the overall score will remain what it was originally.
     */
    public static boolean forceNonNegativeScore = false;

    /**
     * When set to true, the output on all visible tests will be removed, and a corresponding hidden test will
     * be added containing all output for TAs to view.
     */
    public static boolean hideTestsFromStudents = false;

    /**
     * Sets the global Checkstyle configuration options
     * @param filesToCheck a List of file names to run checkstyle on
     * @param checkstyleEnabled true when regular Checkstyle (non-Javadoc) tests should be run
     * @param javadocEnabled true when Javadoc Checkstyle tests should be run
     * @param checkstyleCap maximum number of points a Student can lose from Checkstyle tests
     * @param illegalImportDeduction number of points deducted per illegal import
     * @param illegalUsageDeduction number of points deduced per illegal usage
     * @param allowedImports list of imports permissible for this assignment
     * @param illegalUsages expressions that are not allowed for this assignment
     * @param forceNonNegativeScore when set to true, any result with a negative score will instead have a score of 0.0
     * @param hideTestsFromStudents when set to true, students will be able to see the results of tests but not output
     */
    public static void setGraderConfigOptions(List<String> filesToCheck, boolean checkstyleEnabled,
                                              boolean javadocEnabled, int checkstyleCap, int illegalImportDeduction,
                                              int illegalUsageDeduction, List<String> allowedImports,
                                              List<String> illegalUsages, List<String> allowedCheckstyleErrors,
                                              boolean forceNonNegativeScore, boolean hideTestsFromStudents) {
        GradingUtils.filesToCheck = filesToCheck;
        GradingUtils.checkstyleEnabled = checkstyleEnabled;
        GradingUtils.javadocEnabled = javadocEnabled;
        GradingUtils.checkstyleCap = checkstyleCap;
        GradingUtils.illegalImportDeduction = illegalImportDeduction;
        GradingUtils.illegalUsageDeduction = illegalUsageDeduction;
        GradingUtils.allowedImports = allowedImports;
        GradingUtils.illegalUsages = illegalUsages;
        GradingUtils.allowedCheckstyleErrors = allowedCheckstyleErrors;
        GradingUtils.forceNonNegativeScore = forceNonNegativeScore;
        GradingUtils.hideTestsFromStudents = hideTestsFromStudents;
    }

    public static class CheckstyleResult {
        public final String checkstyleOutput;
        public final int numDeductions;

        public CheckstyleResult(String checkstyleOutput, int numDeductions) {
            this.checkstyleOutput = checkstyleOutput;
            this.numDeductions = numDeductions;
        }
    }

    /**
     * Calls Checkstyle with the given arguments, just as it would be called from command-line
     * @param checkstyleArgs the command-line arguments to supply to Checkstyle
     * @return an deserialized Result containing the full String output as well as the numerical amount of deductions
     * @throws IOException if Checkstyle cannot be properly run or System.exit cannot be disabled
     */
    public static CheckstyleResult getCheckstyleOutput(String[] checkstyleArgs) throws IOException {
        PrintStream oldSysOut = System.out;
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            System.setOut(new PrintStream(outputStream));
            disableSystemExit();
            int numErrors = 0;
            try {
                Main.main(checkstyleArgs);
            } catch (ExitTrappedException sysExitCaught) {
                numErrors = sysExitCaught.numErrors;
            } catch (Throwable t) {
                throw new IOException(t);
            }
            enableSystemExit();
            String originalOutput = outputStream.toString();
            // we assume this method has been called with at least one valid, existing File argument
            File lastFile = new File(checkstyleArgs[checkstyleArgs.length - 1]);
            // need to convert back-slashes to forward slashes in both Strings
            originalOutput = originalOutput.replaceAll("\\\\", "/");
            String toRemove = lastFile.getAbsolutePath().replace(
                lastFile.getName(), "").replaceAll("\\\\", "/"
            );
            // finally, remove all directory information from the output
            originalOutput = originalOutput.replaceAll(toRemove, "");
            if (!allowedCheckstyleErrors.isEmpty()) {
                long originalLength = originalOutput.lines().count();
                long filteredLength = originalOutput.lines().filter(
                    line -> allowedCheckstyleErrors.stream().noneMatch(line::contains)
                ).count();
                if (originalLength != filteredLength) {
                    long newNumErrors = numErrors - (originalLength - filteredLength);
                    String newOutput = originalOutput.lines().filter(
                        line -> allowedCheckstyleErrors.stream().noneMatch(line::contains)
                    ).collect(Collectors.joining("\n"));
                    if (!newOutput.isBlank()) {
                        int index = newOutput.length() - 1;
                        while (index > 0 && newOutput.substring(index).replaceAll("[\\d]+", "").isBlank()) {
                            index--;
                        }
                        newOutput = newOutput.substring(0, index) + "\n" + newNumErrors;
                    }
                    return new CheckstyleResult(newOutput, (int) newNumErrors);
                }
            }
            return new CheckstyleResult(originalOutput, numErrors);
        } catch (Throwable t) {
            throw new IOException(t);
        } finally {
            System.setOut(oldSysOut);
        }
    }

    /**
     * Runs Checkstyle based on the statically configured Checkstyle options
     * @return the entire output of running checkstyle based on the static config options
     * @throws IOException if the main method in the Checkstyle Jar throws any Exception or Error
     */
    public static CheckstyleResult getCheckstyleOutput() throws IOException {
        if (checkstyleEnabled || javadocEnabled) {
            List<String> paramList = filesToCheck.stream().filter(name -> new File(name).exists()).collect(Collectors.toList());
            if (!paramList.isEmpty()) {
                if (checkstyleEnabled && javadocEnabled) {
                    paramList.add(0, "-a");
                } else if (javadocEnabled) {
                    paramList.add(0, "-j");
                }
                return getCheckstyleOutput(paramList.toArray(new String[0]));
            }
        }
        return new CheckstyleResult("Error: Could not find any classes to run Checkstyle on!", -1);
    }

    /**
     * Runs Checkstyle to determine what illegal imports a File list used, based on the static
     * configuration options
     * @return the entire output of running checkstyle to determine illegal imports
     * @throws IOException if the main method in the Checkstyle Jar throws any Exception or Error
     */
    public static CheckstyleResult getIllegalImports() throws IOException {
        String regex = "(^|;|\\s)(java(x|fx)?|org)\\.";
        if (!allowedImports.isEmpty()) {
            regex = String.format("(^|;|\\s)(?!(%s))(java(x|fx)?|org)\\s*\\.", String.join("|", allowedImports));
        }
        return searchStudentFilesForRegex(regex, "Illegal Import");
    }

    private static CheckstyleResult searchStudentFilesForRegex(String regex, String testName) throws IOException {
        int numDeductions = 0;
        StringBuilder builder = new StringBuilder();
        Pattern pattern = Pattern.compile(regex);
        if (!regex.isBlank()) {
            for (String fileName: filesToCheck) {
                int lineNum = 1;
                for (String line: Files.readAllLines(Paths.get(fileName))) {
                    Matcher currMatcher = pattern.matcher(line);
                    while(currMatcher.find()) {
                        numDeductions++;
                        String lineToShow = line.substring(currMatcher.start());
                        builder.append(
                            String.format(
                                "%s on line %d starting with %s for regex %s%n",
                                testName, lineNum, lineToShow, regex
                            )
                        );
                    }
                    lineNum++;
                }
            }
        }
        return new CheckstyleResult(builder.toString(), numDeductions);
    }

    /**
     * Runs Checkstyle to determine what illegal expressions are present in the given File list a File list based
     * on static configuration options
     * @return the entire output of running checkstyle to determine illegal usages
     * @throws IOException if the main method in the Checkstyle 8.28 JAR throws any Exception or Error
     */
    public static CheckstyleResult getIllegalUsages() throws IOException {
        if (!illegalUsages.isEmpty()) {
            return searchStudentFilesForRegex(String.join("|", illegalUsages), "Illegal Usage");
        } else {
            return new CheckstyleResult("", 0);
        }
    }

    /**
     * Runs a custom test Launcher that is thread-safe and does not depend on the
     * idiosyncrasies of JUnit 5, which proved to be problematic in some cases
     * @param classes the list of Class<?> objects from which the test results will be obtained
     * @return the entire JSON output of running the custom GradedTest runner
     */
    public static ObjectNode getOutputFromAllTests(Class<?>[] classes) {
        return GradedTestRunner.runAllTests(classes);
    }

    /**
     * Create a JSON Node with a zero-score, no tests, and a single output message.
     * Should be used in any error case where the autograder fails to execute
     * @param message the overall message to display
     * @return a JSON node as described above
     */
    public static ObjectNode getEmptyGradescopeOutput(String message) {
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        rootNode.set("tests", mapper.createArrayNode());
        rootNode.put("score", 0.0);
        rootNode.put("output", message);
        return rootNode;
    }

    /**
     * Create a Generic TestResult used whenever a Test fails.
     * @param testWrapper an initial wrapper, empty TestResult object to which failure details will be added
     * @param output the String output (usually a stack-trace) generated from the original test failing
     * @return a deserialized TestResult which can easily be converted to a JSON node for the overall JSON output
     */
    public static TestResult getErrorTestOutput(TestResult testWrapper, String output){
        testWrapper.failTest();
        testWrapper.addOutput("Test Failed!\n" + output);
        return testWrapper;
    }

    /**
     * Helper method for runTestSuiteWithArguments - removes the '[' and ']'
     * characters that encapsulate some of the command-line arguments due to the arguments
     * being String conversions of Groovy lists
     * @param args a reference to the entire list of command-line arguments
     * @param toShorten the particular indices to which the above operation should be applied
     */
    private static void shortenIndices(String[] args, List<Integer> toShorten) {
        for (int i: toShorten) {
            if (i < args.length) {
                args[i] = args[i].substring(1, args[i].length() - 1);
            }
        }
    }

    private static List<String> removeBlanks(List<String> stringList) {
        return stringList.stream().filter(str -> !str.isBlank()).collect(Collectors.toList());
    }

    /**
     * Copies over all compiling non-.class Student files whose names do not match a Java file
     * in a given provided directory over to a given temporary directory.
     * @param providedDir directory with a list of zero or more provided Java files
     * @param destDir temporary directory to which we wish to copy student files
     * @throws IOException if any of the given File objects do not exist or are not directories,
     * or if there are any Exceptions encountered when attempting to copy student files to destDir
     */
    public static void copyStudentFiles(File providedDir, File destDir)
            throws IOException {
        for (File dir: List.of(providedDir, destDir)) {
            if (!dir.exists()) {
                throw new IOException("File " + dir.getName() + " does not exist!");
            } else if (!dir.isDirectory()) {
                throw new IOException("File " + dir.getName() + " is not a directory!");
            }
        }
        if (destDir.listFiles() != null && destDir.listFiles().length > 0) {
            for (File destFile: destDir.listFiles()) {
                destFile.delete();
            }
        }
        for (File studentFile: GradingUtils.submittedNonClassFiles) {
            if (providedDir.listFiles() == null ||
                Arrays.stream(providedDir.listFiles()).noneMatch(providedFile -> providedFile.getName().equals(studentFile.getName()))) {
                Files.copy(studentFile.toPath(), Paths.get(destDir.getAbsolutePath() + "/" + studentFile.getName()));
            }
        }
        if (providedDir.listFiles() != null) {
            for (File providedFile: providedDir.listFiles()) {
                if (providedFile.getName().endsWith(".java")) {
                    Files.copy(providedFile.toPath(), Paths.get(destDir.getAbsolutePath() + "/" + providedFile.getName()));
                }
            }
        }
    }

    private static File currentSubmissionDir = null;
    private static List<File> submittedNonClassFiles = new ArrayList<>();

    /**
     * Intended to be called from a main method within an autograder, fed parameters
     * that were serialized from a gradle script to a String array through implicit type conversion
     * @param args the serialized arguments to be de-serialized and used as configuration options
     * @param classes array of Class objects representing the Test Classes to be executed
     */
    public static void runTestSuiteWithArguments(String[] args, Class<?>[] classes) {
        disableSystemExit();
        shortenIndices(args, List.of(3, 9, 10, 11, 14));
        File providedDir = new File(args[0]);
        File destDir = new File(args[1]);
        ReflectHelper.extraClassesDir = args[1];
        File jsonFile = new File(args[2]);
        Map<String, Double> expectedScores = new HashMap<>();
        String[] splitOutput = args[3].split(", ");
        boolean checkingScores = false;
        for (String currSolution: splitOutput) {
            String[] keyPair = currSolution.split(":");
            if (keyPair.length == 1) {
                expectedScores.put(keyPair[0], null);
            } else {
                checkingScores = true;
                expectedScores.put(keyPair[0], Double.parseDouble(keyPair[1]));
            }
        }
        boolean checkstyleEnabled = Boolean.parseBoolean(args[4]);
        boolean javadocEnabled = Boolean.parseBoolean(args[5]);
        int checkstyleCap = Integer.parseInt(args[6]);
        int illegalImportDeduction = Integer.parseInt(args[7]);
        int illegalUsageDeduction = Integer.parseInt(args[8]);
        List<String> allowedImports = args.length < 10 ? List.of() : Arrays.asList(args[9].split(", "));
        List<String> illegalUsages = args.length < 11 ? List.of() : Arrays.asList(args[10].split(", "));
        List<String> allowedCheckstyleErrors = args.length < 12 ? List.of() : Arrays.asList(args[11].split(", "));
        boolean forceNonNegativeScore = args.length >= 13 && Boolean.parseBoolean(args[12]);
        boolean hideTestsFromStudents = args.length >= 14 && Boolean.parseBoolean(args[13]);
        List<String> textReplacements = args.length >= 15 ? Arrays.asList(args[14].split(", ")) : List.of();
        String allPassedMessage = args.length < 16 ? "" : args[15];
        allowedImports = removeBlanks(allowedImports);
        illegalUsages = removeBlanks(illegalUsages);
        allowedCheckstyleErrors = removeBlanks(allowedCheckstyleErrors);
        ObjectNode errorNode = null;
        Map<String, JsonNode> resultMap = new HashMap<>();
        Map<String, Double> actualScores = new HashMap<>();
        try {
            for (String submissionDirName: expectedScores.keySet()) {
                ReflectHelper.classLoader = null;
                GradingUtils.currentSubmissionDir = new File(submissionDirName);
                assertTrue(
                    GradingUtils.currentSubmissionDir.exists(),
                    String.format("Directory %s does not exist!", GradingUtils.currentSubmissionDir.getAbsolutePath())
                );
                assertNotNull(
                    GradingUtils.currentSubmissionDir.listFiles(),
                    String.format(
                        "File %s is either not a directory or is empty!",
                        GradingUtils.currentSubmissionDir.getAbsolutePath()
                    )
                );
                GradingUtils.submittedNonClassFiles =
                    Arrays.stream(GradingUtils.currentSubmissionDir.listFiles())
                          .filter(f -> !f.getName().endsWith(".class")).collect(Collectors.toList());
                if (!destDir.exists()) {
                    destDir.mkdir();
                }
                copyStudentFiles(providedDir, destDir);
                for (String replacement: textReplacements) {
                    String[] split = replacement.split("\t");
                    assertEquals(2, split.length, "Text replacement not formatted properly");
                    if (destDir.listFiles() != null) {
                        for (File f: destDir.listFiles()) {
                            try {
                                String content = Files.readString(f.toPath());
                                content = content.replaceAll(split[0], split[1]);
                                Files.write(f.toPath(), content.getBytes(StandardCharsets.UTF_8));
                            } catch (Throwable ignored) {}
                        }
                    }
                }
                List<File> compilingJavaFiles = Arrays.stream(destDir.listFiles()).filter(f -> f.getName()
                                                      .endsWith(".java")).collect(Collectors.toList());
                // this is very subtle, but in compiling student files here we also make the
                // .class files available for tests to use
                List<String> compilerErrors = getCompilerErrors(compilingJavaFiles);
                compilingJavaFiles = compilingJavaFiles.stream().filter(f -> new File(f.getAbsolutePath()
                                                       .replace(".java", ".class")).exists())
                                                       .collect(Collectors.toList());
                List<String> compilingFileNames =
                    compilingJavaFiles.stream().map(File::getAbsolutePath).collect(Collectors.toList());
                setGraderConfigOptions(
                    compilingFileNames, checkstyleEnabled, javadocEnabled, checkstyleCap,
                    illegalImportDeduction, illegalUsageDeduction, allowedImports, illegalUsages,
                    allowedCheckstyleErrors, forceNonNegativeScore, hideTestsFromStudents
                );
                ObjectNode result = getOutputFromAllTests(classes);
                if (!compilerErrors.isEmpty()) {
                    String currOutput = result.get("output") == null ? "" : result.get("output").asText();
                    result.put("output", currOutput + "\n" + String.join("\n", compilerErrors));
                }
                if (result == null) {
                    String errorMessage =
                        "The autograder could not be run. Most likely, this is because test events were not received."
                            + " If you are a student seeing this message, this likely is happening because something is"
                            + " wrong with the autograder rather than your code, so please contact TAs or the professor";
                    errorNode = getEmptyGradescopeOutput(errorMessage);
                } else {
                    resultMap.put(submissionDirName, result);
                    if (expectedScores.get(submissionDirName) != null) {
                        actualScores.put(submissionDirName, result.get("score").asDouble());
                        double expectedScore = expectedScores.get(submissionDirName);
                        if (!allPassedMessage.isBlank() && Math.abs(expectedScore - result.get("score").asDouble()) <= 0.0001) {
                            String currOutput = result.get("output") == null ? "" : result.get("output").asText();
                            result.put("output", currOutput + allPassedMessage);
                        }
                    }
                }
            }
        } catch (Throwable t) {
            String errorMessage = "There was an error encountered when attempting to execute the autograder: ";
            StringBuilder builder = new StringBuilder(t.toString()).append("\n");
            StackTraceElement[] trace = t.getStackTrace();
            for (StackTraceElement stackTraceElement : trace) {
                builder.append("\tat ").append(stackTraceElement).append("\n");
            }
            errorNode = getEmptyGradescopeOutput(errorMessage + builder.toString());
        }
        enableSystemExit();
        try (PrintWriter printWriter = new PrintWriter(new FileOutputStream(jsonFile))) {
            if (errorNode != null) {
                printWriter.println(errorNode.toPrettyString());
            } else {
                if (resultMap.size() == 1) {
                    printWriter.println(resultMap.values().stream().findFirst().get().toPrettyString());
                } else {
                    ObjectMapper mapper = new ObjectMapper();
                    ObjectNode rootNode = mapper.createObjectNode();
                    resultMap.forEach(rootNode::set);
                    printWriter.println(rootNode.toPrettyString());
                }
            }
            printWriter.flush();
        } catch (FileNotFoundException fnfe) {
            fail(fnfe.toString());
        }
        // TODO uncomment and actually use this code (in addition to the variables that use it) once it is determined
        // why the fail() calls here cause the JVM to spin indefinitely instead of exiting normally.
        /*if (checkingScores) {
            for (String submissionDir : expectedScores.keySet()) {
                if (!actualScores.containsKey(submissionDir)) {
                    fail("Solution " + submissionDir + " did not run!");
                } else {
                    double expected = expectedScores.get(submissionDir);
                    double actual = actualScores.get(submissionDir);
                    if (Math.abs(expected - actual) < 0.0001) {
                        fail(("Solution " + submissionDir + " was supposed to have score " + expected + " but instead had score " + actual));
                    }
                }
            }
        }*/
        System.exit(0);
        /*
         * IMPORTANT: The System.exit call above may seem strangely placed, but it is currently the only
         * known fix for cases in which students submit code that contain an infinite loop and the autograder
         * times out as a result.  Though the loop itself is "handled" through a timeout mechanism, for some
         * reason the program does not exit after the main method is complete, possibly due to some other threads
         * that were spun up from the Future tasks used in GradedTestRunner.
         */
    }

}
