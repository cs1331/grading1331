package grading1331.gradescope;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import grading1331.ConstructorHelper;
import grading1331.Miscellaneous;
import grading1331.ReflectHelper;
import grading1331.builder.GeneratedTestSuite;
import grading1331.builder.RunnableTest;
import grading1331.gradescope.GradingUtils.CheckstyleResult;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.assertTrue;

/**
 * A Wrapper for static methods responsible for running all autograded tests
 * <br>
 * The entry point in this class is runAllTests, as all execution is contained within this method.
 * <br>
 * runAllTests is supplied an Array of Class<?> objects, and it runs each of the tests from those
 * Class-by-Class, extracting the methods with an @GradedTest annotation, as well as properly
 * handing @BeforeEach and @AfterEach.  Each test is run using reflection - very similar to
 * how we actually run student code.  If the method does not error, we say that it passes. As before,
 * weight information and the display name are obtained from the GradedTest annotation.
 * <br>
 * When complete, all information for the cumulative tests, including display names, output, the score, etc
 * gets converted into an overall JSON result - this is what the "ObjectNode" is, a deserialized
 * version of the final JSON result that can be serialized to produce String output gradescope expects.
 * @author Andrew Chafos
 */
public class GradedTestRunner {

    public static int DEFAULT_TIMEOUT = 3000;

    private static TestResult runSingleTask(Callable<TestResult> task, TestResult testWrapper) {
        ExecutorService testExecutor = Executors.newSingleThreadExecutor();
        Future<TestResult> future = testExecutor.submit(task);
        try {
            return future.get(DEFAULT_TIMEOUT, TimeUnit.MILLISECONDS);
        } catch (Throwable t) {
            future.cancel(true);
            return GradingUtils.getErrorTestOutput(testWrapper, getStackTrace(t));
        } finally {
            testExecutor.shutdownNow();
        }
    }

    public static ObjectNode runAllTests(Class<?>[] testClasses) {
        List<TestResult> allTestResults = new ArrayList<>();
        long startTime = System.currentTimeMillis();
        for (Class<?> testClass: testClasses) {
            Method beforeEach = Arrays.stream(testClass.getDeclaredMethods())
                .filter(m -> m.getAnnotation(BeforeEach.class) != null).findFirst().orElse(null);
            Method afterEach = Arrays.stream(testClass.getDeclaredMethods())
                .filter(m -> m.getAnnotation(AfterEach.class) != null).findFirst().orElse(null);
            for (Method method: testClass.getDeclaredMethods()) {
                if (method.getAnnotation(GradedTest.class) != null) {
                    GradedTest gradedTest = method.getAnnotation(GradedTest.class);
                    List<Method> toRun = new ArrayList<>();
                    if (beforeEach != null) {
                        toRun.add(beforeEach);
                    }
                    toRun.add(method);
                    if (afterEach != null) {
                        toRun.add(afterEach);
                    }
                    TestResult wrapper = new TestResult(gradedTest.name(), gradedTest.number(), gradedTest.max_score(), gradedTest.visibility());
                    allTestResults.add(runSingleTask(new GradedTestTask(wrapper, toRun), wrapper));
                }
            }
            if (GeneratedTestSuite.class.isAssignableFrom(testClass)) {
                GeneratedTestSuite instance = (GeneratedTestSuite) ConstructorHelper.forceDefaultConstruction(testClass);
                List<RunnableTest> allTests = instance.getGeneratedTests();
                for (RunnableTest test: allTests) {
                    allTestResults.add(runSingleTask(new GradedTestTask(test.getTestWrapper(), test.getRunnable()), test.getTestWrapper()));
                }
            }
        }
        return afterAll(startTime, allTestResults);
    }

    private static ObjectNode afterAll(long startTime, List<TestResult> allTestResults) {
        long totalTime = System.currentTimeMillis() - startTime;
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode rootNode = mapper.createObjectNode();
        ArrayNode testResults = mapper.createArrayNode();
        if (GradingUtils.hideTestsFromStudents) {
            allTestResults = allTestResults.stream().map(TestResult::asHiddenTests).flatMap(List::stream).collect(Collectors.toList());
        }
        if (GradingUtils.checkstyleEnabled || GradingUtils.javadocEnabled) {
            allTestResults.add(getCheckstyleResult());
        }
        allTestResults.add(getIllegalImportsOutput());
        allTestResults.add(getIllegalUsagesOutput());
        double totalScore = 0.0;
        for (TestResult result: allTestResults) {
            testResults.add(result.getJSONObjectNode(mapper));
            totalScore += result.score;
        }
        rootNode.put("execution_time", totalTime);
        rootNode.set("tests", testResults);
        if (totalScore < 0.0 && GradingUtils.forceNonNegativeScore) {
            rootNode.put("score", 0.0);
        } else {
            rootNode.put("score", totalScore);
        }
        return rootNode;
    }

    public static final int MAX_STACK_TRACE = 100;
    private static boolean alreadyDisabled = false;

    public static String getStackTrace(Throwable t) {
        try {
            if (!alreadyDisabled) {
                Miscellaneous.disableAccessWarnings();
                alreadyDisabled = true;
            }

            Field depthField = ReflectHelper.getDeclaredField(Throwable.class, "depth");
            int depth = (int) ReflectHelper.getFieldValue(depthField, t);
            StringBuilder builder = new StringBuilder(t.toString());
            StackTraceElement[] stackTrace;
            if (depth > MAX_STACK_TRACE) {
                Method stackTraceMethod = ReflectHelper.getDeclaredMethod(StackTraceElement.class, "of", Throwable.class, int.class);
                ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(stackTraceMethod, (Object) null, t, MAX_STACK_TRACE);
                assertTrue(mir.returnValue.isPresent(), "We were unable to get the Stack Trace for this method");
                stackTrace = (StackTraceElement[]) mir.returnValue.get();
            } else {
                stackTrace = t.getStackTrace();
            }
            for (StackTraceElement traceElement: stackTrace) {
                builder.append("\n\tat ").append(traceElement);
            }
            return builder.toString();
        } catch (Throwable m) {
            return "Caught error trying to get the stacktrace";
        }
    }

    /**
     * A callable task encapsulating an autograder test, which can also be supplied with an @BeforeEach test
     * and/or @AfterEach test to be run in addition to the main test.
     * This Task is used in order to execute all the tests asynchronously - namely, so that one can be timed out
     * if it enters an infinite loop.
     */
    private static class GradedTestTask implements Callable<TestResult> {
        private final Object testObject;
        private final TestResult testResultWrapper;
        private final List<Method> methodsToRun;
        private final Runnable runnable;

        public GradedTestTask(TestResult testResultWrapper, List<Method> methodsToRun) {
            if (methodsToRun.isEmpty()) throw new IllegalArgumentException("Must run at least 1 method!");
            this.testObject = ConstructorHelper.forceDefaultConstruction(methodsToRun.get(0).getDeclaringClass());
            this.testResultWrapper = testResultWrapper;
            this.methodsToRun = methodsToRun;
            this.runnable = null;
        }

        public GradedTestTask(TestResult testResultWrapper, Runnable runnable) {
            this.testObject = this;
            this.testResultWrapper = testResultWrapper;
            this.methodsToRun = List.of(ReflectHelper.getDeclaredMethod(GradedTestTask.class, "runRunnable"));
            this.runnable = runnable;
        }

        public void runRunnable() {
            runnable.run();
        }

        @Override
        public TestResult call() {
            TestResult currResult = testResultWrapper;
            int numSucceeded = 0;
            StringBuilder allOutput = new StringBuilder();
            for (Method method: methodsToRun) {
                ReflectHelper.MethodInvocationReport report = ReflectHelper.invokeMethod(true, method, testObject);
                allOutput.append(report.sysoutCapture.toString());
                if (report.exception.isPresent() || !report.syserrCapture.toString().isBlank()) {
                    allOutput.append("Test Failed!\n").append(report.syserrCapture.toString());
                    report.exception.ifPresent(throwable -> allOutput.append(getStackTrace(throwable)));
                    break; // As soon as one method fails, we have no need to proceed with the main test and/or @AfterEach
                } else {
                    numSucceeded++;
                }
            }
            if (numSucceeded > 0 && numSucceeded == methodsToRun.size()) {
                currResult.passTest();
            }
            String outputString = allOutput.toString();
            int MAX_OUTPUT_LENGTH = 8192;
            if (outputString.length() > MAX_OUTPUT_LENGTH) {
                outputString = outputString.substring(0, MAX_OUTPUT_LENGTH) + "... truncated due to excessive output!";
            }
            currResult.addOutput(outputString);
            return currResult;
        }
    }

    private static TestResult getCheckstyleResult(String testName, int deductionWeight) {
        boolean checkstyleFailed = false;
        TestResult testResult = new TestResult(testName, "", 0, Visibility.VISIBLE);
        int numDeductions = -1;
        try {
            CheckstyleResult result;
            if ("Illegal Imports".equals(testName)) {
                result = GradingUtils.getIllegalImports();
            } else if ("Illegal Usages".equals(testName)) {
                result = GradingUtils.getIllegalUsages();
            } else {
                result = GradingUtils.getCheckstyleOutput();
            }
            if (result.numDeductions >= 0) {
                numDeductions = result.numDeductions;
            }
            testResult.addOutput(result.checkstyleOutput);
        } catch (IOException ioe) {
            checkstyleFailed = true;
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ioe.printStackTrace(new PrintStream(outputStream));
            testResult.addOutput("Got " + ioe.getMessage() + " trying to run checkstyle!");
            testResult.addOutput(outputStream.toString());
        }
        if (checkstyleFailed || numDeductions < 0) {
            if ("Checkstyle".equals(testName)) {
                testResult.score = -GradingUtils.checkstyleCap;
            } else {
                testResult.score = -deductionWeight;
            }
        } else {
            if ("Checkstyle".equals(testName)) {
                testResult.score = Math.max(-numDeductions * deductionWeight, -Math.abs(GradingUtils.checkstyleCap));
            } else {
                testResult.score = -numDeductions * deductionWeight;
            }
        }
        return testResult;
    }

    private static TestResult getCheckstyleResult() {
        return getCheckstyleResult("Checkstyle", 1);
    }

    private static TestResult getIllegalImportsOutput() {
        return getCheckstyleResult("Illegal Imports", GradingUtils.illegalImportDeduction);
    }

    private static TestResult getIllegalUsagesOutput() {
        return getCheckstyleResult("Illegal Usages", GradingUtils.illegalUsageDeduction);
    }
}
