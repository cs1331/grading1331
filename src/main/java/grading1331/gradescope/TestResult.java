package grading1331.gradescope;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents the Result of an individual Unit Test in an Autograder Test Class.
 * <br>
 * Contains a utility method to generate a JSON representation of this result
 * @author Andrew Chafos
 */
public class TestResult {
    private final String testName;
    private final String number;
    private final double maxScore;
    public double score;
    private final Visibility visibility;
    private StringBuilder cumulativeOutput;

    /**
     * Constructs a new TestResult object with a given test name, number, maxScore, and visibility level,
     * all of which will be constant fields.
     * @param testName the visible display name of the current test
     * @param number the number of the test on Gradescope
     * @param maxScore the score the student achieves for this test if it passes
     * @param visibility the visibility level of this test, see {@link grading1331.gradescope.Visibility}
     */
    public TestResult(String testName, String number, double maxScore, Visibility visibility) {
        this.testName = testName;
        this.number = number;
        this.maxScore = maxScore;
        this.visibility = visibility;
        this.score = 0.0;
        this.cumulativeOutput = new StringBuilder();
    }

    /**
     * When invoked, will set the student's score to the maxScore, indicating they passed the current test.
     */
    public void passTest() {
        this.score = this.maxScore;
    }

    /**
     * Used to ensure the current score of this TestResult object is 0 when constructing an empty test with
     * failure information via {@link grading1331.gradescope.GradingUtils#getErrorTestOutput(TestResult, String)}
     */
    public void failTest() {
        this.score = 0.0;
    }

    /**
     * Adds the given newOutput String to the cumulative output
     * @param newOutput the String to tack on to the cumulative String output
     */
    public void addOutput(String newOutput) {
        cumulativeOutput.append(newOutput);
    }

    /**
     * Converts this TestResult to a one of the same visibility with no output, as well as
     * another that is strictly hidden with all of the current output
     */
    public List<TestResult> asHiddenTests() {
        List<TestResult> results = new ArrayList<>();
        results.add(new TestResult(testName, number, maxScore, visibility)); // student one
        if (this.score == this.maxScore) {
            results.get(0).passTest();
        }
        results.add(new TestResult(testName, number, 0.0, Visibility.HIDDEN));
        results.get(1).addOutput(cumulativeOutput.toString());
        return results;
    }

    /**
     * Generates a JSON Object representation of this TestResult object
     * @param mapper the ObjectMapper needed to create a new ObjectNode object
     * @return a new JSON ObjectNode that represents the current TestResult
     */
    public ObjectNode getJSONObjectNode(ObjectMapper mapper) {
        ObjectNode testNode = mapper.createObjectNode();
        testNode.put("name", testName);
        testNode.put("number", number);
        testNode.put("score", score);
        testNode.put("max_score", maxScore);
        testNode.put("visibility", visibility.name().toLowerCase());
        testNode.put("output", cumulativeOutput.toString());
        return testNode;
    }

}
