package grading1331;

import javassist.util.proxy.MethodHandler;
import javassist.util.proxy.ProxyFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.PrintStream;
import java.lang.reflect.*;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.objectweb.asm.*;
import org.objectweb.asm.tree.*;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Reflection helper.
 *
 * @author jdierberger3, jkelly80, nzhong31
 * @version 0.8 (documentation)
 */
public final class ReflectHelper {

    /**
     * dnu.
     */
    private ReflectHelper() { }

    public static String extraClassesDir = "";
    public static URLClassLoader classLoader = null;

    /**
     * Get a Class, or fail if it does not exist.
     * @param name The name of the Class to get.
     * @return The Class object from the given name.
     */
    public static Class<?> getClass(String name) {
        if (!extraClassesDir.isBlank()) {
            try {
                if (classLoader == null) {
                    classLoader = new URLClassLoader(
                        new URL[]{new File(extraClassesDir).toURI().toURL()}
                    );
                }
                return Class.forName(name, true, classLoader);
            } catch (Exception ignored) {}
        }
        try {
            return Class.forName(name);
        } catch (ClassNotFoundException e) {
            fail("Could not find class: " + name + ". Does it compile correctly?");
        } catch (NoClassDefFoundError ncdfe) {
            fail("A class which the " + name + " class depends on does not exist: " + ncdfe);
        }
        return null;
    }

    /**
     * Get a Method from the given class, or fail if one does not exist. This
     * method can obtain non-public methods as well.
     * @param cls The name of the class to get the Method from.
     * @param name The name of the Method to get.
     * @param paramTypes The parameter types of the target Method.
     * @return The Method obtained.
     */
    public static Method getDeclaredMethod(String cls, String name, Class<?>... paramTypes) {
        return getDeclaredMethod(getClass(cls), name, paramTypes);
    }

    /**
     * Get a Method from the given class, or fail if one does not exist. This
     * method can obtain non-public methods as well.
     * @param cls The class to get the Method from.
     * @param name The name of the Method to get.
     * @param paramTypes The parameter types of the target Method.
     * @return The Method obtained.
     */
    public static Method getDeclaredMethod(Class<?> cls, String name, Class<?>... paramTypes) {
        Method mtd = null;
        try {
            mtd = cls.getDeclaredMethod(name, paramTypes);
            mtd.setAccessible(true);
        } catch (NoSuchMethodException e) {
            fail("Could not find declared method: " + name + "(" + Arrays.toString(paramTypes) + ")");
        }
        return mtd;
    }

    /**
     * Get a Field from the given class, or fail if one does not exist. This
     * method can obtain non-public fields as well.
     * @param cls The name of the class to get the Field from.
     * @param name The name of the Field to get.
     * @return The Field obtained.
     */
    public static Field getDeclaredField(String cls, String name) {
        return getDeclaredField(getClass(cls), name);
    }

    /**
     * Get a Field from the given class, or fail if one does not exist. This
     * method can obtain non-public fields as well.
     * @param cls The class to get the Field from.
     * @param name The name of the Field to get.
     * @return The Field obtained.
     */
    public static Field getDeclaredField(Class<?> cls, String name) {
        Field mtd = null;
        try {
            mtd = cls.getDeclaredField(name);
            mtd.setAccessible(true);
        } catch (NoSuchFieldException e) {
            fail("Could not find declared field: " + name);
        }
        return mtd;
    }

    /**
     * Get a Constructor from the given class, or fail if one does not exist. This
     * method can obtain non-public Constructors as well.
     * @param cls The name of the class to get the Constructor from.
     * @param paramTypes The parameter types of the target Constructor.
     * @return The Constructor obtained.
     */
    public static Constructor<?> getDeclaredConstructor(String cls, Class<?>... paramTypes) {
        return getDeclaredConstructor(getClass(cls), paramTypes);
    }

    /**
     * Get a Constructor from the given class, or fail if one does not exist. This
     * method can obtain non-public Constructors as well.
     * @param cls The class to get the Constructor from.
     * @param paramTypes The parameter types of the target Constructor.
     * @return The Constructor obtained.
     */
    public static Constructor<?> getDeclaredConstructor(Class<?> cls, Class<?>... paramTypes) {
        Constructor<?> mtd = null;
        try {
            mtd = cls.getDeclaredConstructor(paramTypes);
            mtd.setAccessible(true);
        } catch (NoSuchMethodException e) {
            fail("Could not find declared constructor: (" + Arrays.toString(paramTypes) + ")");
        }
        return mtd;
    }

    /**
     * Method invocation reports contain all details of the result of invoking
     * a method.
     *
     * @author jdierberger1
     * @version 1.0
     */
    public static final class MethodInvocationReport {
        /**
         * Name of the method being invoked.
         */
        public final String methodName;
        /**
         * The ByteArrayOuputStream which captured System.out
         */
        public final ByteArrayOutputStream sysoutCapture;
        /**
         * The ByteArrayOutputStream which captured System.err
         */
        public final ByteArrayOutputStream syserrCapture;
        /**
         * An Optional containing any caught Throwables (if they exist).
         */
        public final Optional<Throwable> exception;
        /**
         * An Optional containing the return value. No distinction is made
         * between a void method and returning null.
         */
        public final Optional<Object> returnValue;

        /**
         * Make a MethodInvocationReport.
         * @param name The method name.
         * @param sysout The sysout capture.
         * @param syserr The syserr capture.
         * @param exception The exception caught.
         * @param ret The return value.
         */
        private MethodInvocationReport(String name, ByteArrayOutputStream sysout,
                ByteArrayOutputStream syserr, Throwable exception,
                Object ret) {
            methodName = name;
            sysoutCapture = sysout;
            syserrCapture = syserr;
            this.exception = Optional.ofNullable(exception);
            returnValue = Optional.ofNullable(ret);
        }
    }

    /**
     * Get a Class[] indicating the parameter types for the given parameters.
     * @param params The parameters to get the types for.
     * @return The Class[] indicating the types of the parameters given.
     */
    public static Class<?>[] getParamTypesFromParams(Object... params) {
        Class<?>[] paramTypes = new Class<?>[params.length];
        for (int i = 0; i < paramTypes.length; i++) {
            if (params[i] == null) {
                paramTypes[i] = Object.class;
            } else {
                paramTypes[i] = params[i].getClass();
            }
        }
        return paramTypes;
    }

    /**
     * Invoke a method reflectively and get the MethodInvocationReport
     * containing information about the invocation. See
     * invokeMethod(Method, Object, Object...) for more.
     * @param cls The name of the class to get.
     * @param name The name of the method to invoke.
     * @param target The Object to invoke the method on, or null if it is a
     * static method.
     * @param params The parameters to invoke the method with.
     * @return The MethodInvocationReport for the invocation.
     */
    public static MethodInvocationReport invokeMethod(Class<?> cls, String name, Object target, Object... params) {
        return invokeMethod(getDeclaredMethod(cls, name, getParamTypesFromParams(params)), target, params);
    }


    public static MethodInvocationReport invokeMethod(boolean suppressAllErrors, Method m, Object target,
                                                      Object... params) {
        return invokeMethod(suppressAllErrors, m, "", target, params);
    }

    /**
     * Invoke a method reflectively and get the MethodInvocationReport
     * containing information about the invocation. System.out and System.err
     * will be captured temporarily so that console prints can be inspected.
     * They are restored post-invocation. If an InvocationTargetException is
     * caught, and the exception wrapped is  an Error, that Error will be
     * re-thrown (in case it is an AssertionError or other critical error).
     * Otherwise it will be captured and returned. All other Exceptions will
     * induce a failure.
     * @param m The method to invoke.
     * @param target The target object to invoke the method on, or null if it
     * is a static method.
     * @param params The parameters to use in the invocation.
     * @param suppressAllErrors a boolean flag specifying whether or not we should suppress errors
     *                          that are thrown
     * @return The MethodInvocationReport containing all relevant information
     * about the invocation of the method.
     */
    public static synchronized MethodInvocationReport invokeMethod(boolean suppressAllErrors, Method m, String sysIn, Object target, Object... params) {
        PrintStream sysOut = System.out;
        PrintStream sysErr = System.err;
        InputStream originalSysIn = System.in;
        ByteArrayOutputStream captureOut = new ByteArrayOutputStream();
        ByteArrayOutputStream captureErr = new ByteArrayOutputStream();
        Throwable thrown = null;
        Object retVal = null;
        try {
            System.setIn(new ByteArrayInputStream(sysIn.getBytes()));
            System.setOut(new PrintStream(captureOut));
            System.setErr(new PrintStream(captureOut));
            retVal = m.invoke(target, params);
        } catch (InvocationTargetException e) {
            // originating in the method invoked
            Throwable t = e;
            t = t.getCause();
            // propagate up errors
            if (t instanceof Error && !suppressAllErrors) {
                throw (Error) t;
            }
            // otherwise log it
            thrown = t;
        } catch (Throwable e) {
            e.printStackTrace(sysErr);
            fail("Got " + e.getClass().getSimpleName() + " invoking " + m.getName() + " (abnormal)");
        } finally {
            System.setOut(sysOut);
            System.setErr(sysErr);
            System.setIn(originalSysIn);
        }
        return new MethodInvocationReport(m.getName(), captureOut, captureErr, thrown, retVal);
    }

    public static MethodInvocationReport invokeMethod(Method m, String sysIn, Object target, Object... params) {
        return invokeMethod(false, m, sysIn, target, params);
    }

    /**
     * Calls invokeMethod with suppressAllErrors set to false.
     * @param m the method to be invoked
     * @param target the target Object on which the method will be invoked
     * @param params method parameters that will be passed to the invocation
     * @return the corresponding MethodInvocationReport from invokeMethod
     */
    public static MethodInvocationReport invokeMethod(Method m, Object target, Object... params) {
        return invokeMethod(false, m, target, params);
    }

    /**
     * Set the value of a field.
     * @param name The name of the Field to set the value of.
     * @param cls The class containing the Field.
     * @param target The target object to set the value in.
     * @param newValue The new value to set to.
     * @return If the set was successful.
     */
    public static boolean setField(String name, Class<?> cls, Object target, Object newValue) {
        try {
            Field field = getDeclaredField(cls, name);
            field.set(target, newValue);
            return true;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            return false;
        }
    }

    /**
     * Set the value of a field.
     * @param field The Field to set.
     * @param target The target object to set the value in.
     * @param newValue The new value to set to.
     * @return If the set was successful.
     */
    public static boolean setField(Field field, Object target, Object newValue) {
        try {
            field.setAccessible(true);
            field.set(target, newValue);
            return true;
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            return false;
        }
    }

    /**
     * Get the value of a field.
     * @param name The name of the Field to get the value of.
     * @param cls The class containing the Field.
     * @param target The target object to get the value in.
     * @return The value of the field, failing if it could not be acquired.
     */
    public static Object getFieldValue(String name, Class<?> cls, Object target) {
        try {
            Field field = getDeclaredField(cls, name);
            return field.get(target);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            fail("Couldn't get value of field " + name);
            return null;
        }
    }

    /**
     * Get the value of a field.
     * @param field The Field to get.
     * @param target The target object to get the value in.
     * @return The value of the field, failing if it could not be acquired.
     */
    public static Object getFieldValue(Field field, Object target) {
        try {
            field.setAccessible(true);
            return field.get(target);
        } catch (SecurityException | IllegalArgumentException | IllegalAccessException e) {
            fail("Couldn't get value of field " + field.getName());
            return null;
        }
    }

    /**
     * Get an instance of a Class using a supplied constructor Object
     * <br>
     * This should NOT be used to get an instance of an object in general!!! It should only
     * be used when we wish to test constructor functionality.
     * @param constr the Constructor<?> object from which the instance will be obtained
     * @param constructorParams The parameters to give to the Constructor.
     * @return The instance from that Constructor.
     */
    public static Object getInstance(Constructor<?> constr, Object...constructorParams) {
        try {
            return constr.newInstance(constructorParams);
        } catch (InvocationTargetException e) {
            fail("Got " + e.getCause().getClass().getSimpleName() + " constructing instance of "
                    + constr.getName());
            return null;
        } catch (InstantiationException | IllegalAccessException | IllegalArgumentException e) {
            fail("Could not construct instance of " + constr.getName() + ": "
                + e.getClass().getSimpleName() + " " + e.getMessage());
            return null;
        }
    }

    /**
     * Gets an instance of the given class with name className by invoking {@link ConstructorHelper#forceDefaultConstruction(Class)},
     * and then sets each of the fields with names given by fieldNames to the corresponding values given by fieldValues
     * @param className the name of the class of which an instance is being obtained
     * @param fieldNames the names of the fields which we wish to set
     * @param fieldValues the values that will be given to the set fields, in order
     * @return an instance with fields set as described above
     */
    public static Object getInstance(String className, String[] fieldNames, Object[] fieldValues) {
        return getInstance(getClass(className), fieldNames, fieldValues);
    }

    /**
     * Gets an instance of the given class object by invoking {@link ConstructorHelper#forceDefaultConstruction(Class)},
     * and then sets each of the fields with names given by fieldNames to the corresponding values given by fieldValues
     * @param cls the class of which an instance is being obtained
     * @param fieldNames the names of the fields which we wish to set
     * @param fieldValues the values that will be given to the set fields, in order
     * @return an instance with fields set as described above
     */
    public static Object getInstance(Class<?> cls, String[] fieldNames, Object[] fieldValues) {
        Object instance = ConstructorHelper.forceDefaultConstruction(cls);
        if (fieldNames.length != fieldValues.length) {
            throw new IllegalArgumentException("Number of fields and number of corresponding values to set must be equal!");
        }
        for (int i = 0; i < fieldNames.length; i++) {
            setField(fieldNames[i], cls, instance, fieldValues[i]);
        }
        return instance;
    }

    /**
     * Gets an instance of the given class with name className by invoking {@link ConstructorHelper#forceDefaultConstruction(Class)},
     * and then sets each of the fields given by fieldValueMap by their corresponding values in the map
     * @param className the name of the class of which an instance is being obtained
     * @param fieldValueMap a mapping from field names to the Object values to which they will be set
     * @return an instance with fields set as described above
     */
    public static Object getInstance(String className, Map<String, Object> fieldValueMap) {
        return getInstance(getClass(className), fieldValueMap);
    }

    /**
     * Gets an instance of the given class with name className by invoking {@link ConstructorHelper#forceDefaultConstruction(Class)},
     * and then sets each of the fields given by fieldValueMap by their corresponding values in the map
     * @param cls the class of which an instance is being obtained
     * @param fieldValueMap a mapping from field names to the Object values to which they will be set
     * @return an instance with fields set as described above
     */
    public static Object getInstance(Class<?> cls, Map<String, Object> fieldValueMap) {
        Object instance = ConstructorHelper.forceDefaultConstruction(cls);
        for (String fieldName: fieldValueMap.keySet()) {
            setField(fieldName, cls, instance, fieldValueMap.get(fieldName));
        }
        return instance;
    }

    /**
     * Get an instance of a Class without explicitly setting any field values
     * <br>
     * Note that this will produce some object whose fields are initialized by one of the student constructors,
     * meaning their end values are not deterministic.
     * @param cls The Class to get the instance from.
     * @return The instance from that class.
     */
    public static Object getInstance(Class<?> cls) {
        return getInstance(cls, new String[0], new Object[0]);
    }

    /**
     * Get an instance of a Class without explicitly setting any field values
     * <br>
     * Note that this will produce some object whose fields are initialized by one of the student constructors,
     * meaning their end values are not deterministic.
     * @param cls The name of the Class to get the instance from
     * @return The instance from that class.
     */
    public static Object getInstance(String cls) {
        return getInstance(getClass(cls), new String[0], new Object[0]);
    }

    /**
     * A length zero array for when no arguments need to be given to main.
     */
    public static final String[] NOARGS = new String[0];

    /**
     * Test the main method of a class.
     * @param className The name of the class to test the main method of.
     * @param args The arguments to pass into the main method.
     * @return The String from System.out's output.
     */
    public static String testMain(String className, String[] args) {
        return testMain(getClass(className), args);
    }

    /**
     * Test the main method of a class.
     * @param className The name of the class to test the main method of.
     * @param args The arguments to pass into the main method.
     * @param systemIn The String input to give as System.in.
     * @return The String from System.out's output.
     */
    public static String testMain(String className, String[] args, String systemIn) {
        return testMain(getClass(className), args, systemIn);
    }

    /**
     * Test the main method of a class.
     * @param cls The class to test the main method of.
     * @param args The arguments to pass into the main method.
     * @return The String from System.out's output.
     */
    public static String testMain(Class<?> cls, String[] args) {
        return testMain(cls, args, "");
    }

    /**
     * Test the main method of a class.
     * @param cls The class to test the main method of.
     * @param args The arguments to pass into the main method.
     * @param systemIn The String input to give as System.in.
     * @return The String from System.out's output.
     */
    public static String testMain(Class<?> cls, String[] args, String systemIn) {
        Method main = getDeclaredMethod(cls, "main", String[].class);
        Object instance = ConstructorHelper.forceDefaultConstruction(cls);
        MethodInvocationReport mir = invokeMethod(main, systemIn, instance, (Object) args);
        mir.exception.ifPresent(
            throwable -> fail(
                "Main method in class " + cls.getSimpleName() + " did not run correctly (got " + throwable + ")"
            )
        );
        return mir.sysoutCapture.toString();
    }

    /**
     * This method recursively searches for all fields a class might have. Including inherited ones.
     * Unlike the getField method this method will find private fields as well as public ones. This
     * method also sets the field it returns to be assecible.
     * @param cls the class to test the main method of
     * @param name the name of the field to search for
     * @return The Field it finds
     */
    public static Field getFieldWithInheritance(Class<?> cls, String name) {
        if (cls == null) {
            fail("You do not have the field " + name);
        }
        Field mtd;
        try {
            mtd = cls.getDeclaredField(name);
            mtd.setAccessible(true);
        } catch (NoSuchFieldException e) {
            return getFieldWithInheritance(cls.getSuperclass(), name);
        }
        return mtd;
    }

    /**
     * Checks if method with correct name and params exists. If so, returns the method.
     * @param className class name, ex 'Bird'
     * @param methodName method name, ex. 'calculateAverage'
     * @param params method parameters in correct order
     * @return specified method, if it exists
     **/
    public static Method grabMethod(Class<?> className, String methodName, Class<?>... params) {
        String message = "Method " + methodName + " is missing, incorrectly named, or has incorrect parameters.";
        ReflectAssert.assertMethodExists(message, className, methodName, params);
        return ReflectHelper.getDeclaredMethod(className, methodName, params);
    }

    /**
     * Checks if field with correct name and type exists. If so, returns the field.
     * @param className class name, ex 'Bird'
     * @param fieldName field name, ex. 'maximumValue'
     * @param fieldType field type, ex. 'int'
     * @return specified field, if it exists
     **/
    public static Field grabField(Class<?> className, String fieldName, Class<?> fieldType) {
        ReflectAssert.assertFieldExists(fieldName + " missing or incorrectly named.", className, fieldName);
        Field field = ReflectHelper.getDeclaredField(className, fieldName);
        ReflectAssert.assertFieldType(fieldName + " is not a " + fieldType.getName(), field, fieldType);
        return field;
    }

    /**
     * Set of all primitive types in Java
     */
    public static final Set<Class<?>> primitiveTypes = Set.of(
        boolean.class, byte.class, short.class, int.class, long.class, char.class, float.class, double.class
    );

    /**
     * Set of all primitive wrapper types in Java
     */
    public static final Set<Class<?>> wrapperTypes = Set.of(
        Boolean.class, Byte.class, Short.class, Integer.class, Long.class, Character.class, Float.class, Double.class
    );

    /**
     * Set of all primitive & primitive wrapper types in Java
     */
    public static final Set<Class<?>> primitiveAndWrapperTypes;
    static {
        primitiveAndWrapperTypes = new HashSet<>(primitiveTypes);
        primitiveAndWrapperTypes.addAll(wrapperTypes);
    }

    /**
     * Provides check for getter, methodName must follow naming convention, getVariableName()
     * This does NOT check to see if the getter is public or non-static
     * @param instance the instance of the class you are testing
     * @param variableName the name of the variable that has a getter
     * @param value value retrieved by the getter, make non-default. ex, if int, don't make 0
     **/
    public static void checkGetter(Object instance, String variableName, Object value) {
        Field field = ReflectHelper.getDeclaredField(instance.getClass(), variableName);
        String methodName = "get" + variableName.substring(0, 1).toUpperCase() + variableName.substring(1);
        Method method = grabMethod(instance.getClass(), methodName);
        try {
            field.set(instance, value);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            fail(e.getClass().getSimpleName() + ": " + e.getMessage() + "\n\nUnable to set field. Is the field final?");
        }
        MethodInvocationReport mir = ReflectHelper.invokeMethod(method, instance);
        assertTrue(mir.returnValue.isPresent(), methodName + " should have a return value");
        ReflectAssert.assertSameSafely(value, mir.returnValue.get(), (methodName + " does not return the correct value"));
    }

    /**
     * Provides check for setter, methodName must follow naming convention, setVariableName()
     * This does NOT check to see if the setter is public or non-static
     * @param instance the instance of the class you are testing
     * @param variableName the name of the variable that has a setter
     * @param type variable type
     * @param startValue value variable is originally set to
     * @param setValue value the variable is set to using setter
     **/
    public static void checkSetter(Object instance, String variableName, Class<?> type, Object startValue, Object setValue) {
        Field field = ReflectHelper.getDeclaredField(instance.getClass(), variableName);
        String methodName = "set" + variableName.substring(0, 1).toUpperCase() + variableName.substring(1);
        Method method = grabMethod(instance.getClass(), methodName, type);
        try {
            field.set(instance, startValue);
            MethodInvocationReport mir = ReflectHelper.invokeMethod(method, instance, setValue);
            if (mir.exception.isPresent()) {
                Throwable t = mir.exception.get();
                fail(method + " threw an exception: " + t.getClass().getSimpleName() + ": " + t.getMessage());
            }
            assertFalse(mir.returnValue.isPresent(), "Return value present for " + methodName + "; should be void.");
            ReflectAssert.assertSameSafely(setValue, field.get(instance), variableName + " not set in " + methodName);
        } catch (IllegalArgumentException | IllegalAccessException e) {
            fail(e.getClass().getSimpleName() + ": " + e.getMessage()
                 + "\n\nUnable to set field. Is the field final?");
        }
    }

    /**
     * Creates an instance of an abstract class given a list of parameters
     * @param abstractClass the class object of the abstract class we wish to instantiate
     * @param methodFunctionMap a mapping from Method objects to code snippets that will be run when said methods are invoked
     * @param paramTypes list of Class<?> object representing the parameter types of params
     * @param params a parameter list with which the abstract class will be instantiated
     * @return a concrete object extending the abstract class
     */
    public static Object getAbstractClassInstance(Class<?> abstractClass, Map<Method, Function<Object[], Object>> methodFunctionMap, Class<?>[] paramTypes, Object... params) {
        Miscellaneous.disableAccessWarnings();
        try {
            ProxyFactory factory = new ProxyFactory();
            factory.setSuperclass(abstractClass);
            return factory.create(paramTypes, params, new AbstractMethodHandler(abstractClass, methodFunctionMap));
        } catch (Exception e) {
            String error = e.toString();
            fail("Could not create a concrete instance of abstract class " + abstractClass.getSimpleName() + " due to " + error);
            return null;
        }
    }

    private static class AbstractMethodHandler implements InvocationHandler, MethodHandler {
        private final Map<MethodKey, Function<Object[], Object>> methodFunctionMap;
        private final Class<?> nonConreteClass;

        public AbstractMethodHandler(Class<?> nonConcreteClass, Map<Method, Function<Object[], Object>> methodFunctionMap) {
            this.nonConreteClass = nonConcreteClass;
            this.methodFunctionMap = methodFunctionMap.entrySet().stream().collect(
                Collectors.toMap(entry -> new MethodKey(entry.getKey()), Map.Entry::getValue)
            );
        }

        private static class MethodKey {
            private final Method method;

            public MethodKey(Method method) {
                this.method = method;
            }

            public boolean equals(Object other) {
                if (!(other instanceof MethodKey)) return false;
                Method otherMethod = ((MethodKey) other).method;
                return otherMethod.getName().equals(method.getName())
                    && Arrays.equals(otherMethod.getParameterTypes(), method.getParameterTypes())
                    && otherMethod.getReturnType().equals(method.getReturnType());
            }

            public int hashCode() {
                int hash = 31;
                hash *= 31 * hash + method.getName().hashCode();
                hash *= 31 * hash + Arrays.hashCode(method.getParameterTypes());
                hash *= 31 * hash + method.getReturnType().hashCode();
                return hash;
            }
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) {
            MethodKey key = new MethodKey(method);
            if (!methodFunctionMap.containsKey(key)) {
                fail("Could not find method " + method.toString() + " in map " + methodFunctionMap);
                return null;
            } else {
                return methodFunctionMap.get(key).apply(args);
            }
        }

        @Override
        public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) {
            MethodKey key = new MethodKey(thisMethod);
            if (!methodFunctionMap.containsKey(key)) {
                boolean succeeded = false;
                if (!Modifier.isAbstract(thisMethod.getModifiers())) {
                    MethodInvocationReport mir = ReflectHelper.invokeMethod(proceed, self, args);
                    String paramString;
                    String throwableString = "";
                    try {
                        paramString = Arrays.toString(args);
                        throwableString = mir.exception.isPresent() ? mir.exception.get().toString() : "";
                    } catch (Throwable t) {
                        paramString = String.format("{args of length %d}", args == null ? 0 : args.length);
                    }
                    assertFalse(
                        mir.exception.isPresent(),
                        "Got an exception when trying to invoke method " + proceed + " with params " + paramString + ": " + throwableString
                    );
                    if (!mir.sysoutCapture.toString().isEmpty()) {
                        System.out.println(mir.sysoutCapture);
                    }
                    succeeded = true;
                    if (mir.returnValue.isPresent()) {
                        return mir.returnValue.get();
                    }
                }
                if (!succeeded) {
                    fail("Could not find method " + proceed + " in map " + methodFunctionMap);
                }
                return null;
            } else {
                return methodFunctionMap.get(key).apply(args);
            }
        }
    }

    /**
     * Creates an instance of an abstract class given a list of parameters
     * @param interfaceClass the class Object of the interface we desire an instance of
     * @param methodFunctionMap a mapping from methods to functions which take in parameter lists
     *                          and return some value
     * @return a concrete instance of the interface whose methods will have behavior defined by methodFunctionMap
     */
    public static Object getInterfaceInstance(Class<?> interfaceClass, Map<Method, Function<Object[], Object>> methodFunctionMap) {
        Miscellaneous.disableAccessWarnings();
        return Proxy.newProxyInstance(
                interfaceClass.getClassLoader(), new Class<?>[]{interfaceClass}, new AbstractMethodHandler(interfaceClass, methodFunctionMap)
        );
    }

    /**
     * Method that retrieves local variable from within a method. Accomplishes this through the Java ASM package which
     * provides access to the .class bytecode.
     * @param fileName Name of the file that we are parsing the bytecode of.
     * @param classObject Class object, typically declared in the testing file
     * @param methodName Name of the method we are looking into.
     * @param variableName Name of the variable we are looking for.
     * @return variable we are looking for, else empty
     */
    public static String getLocalVariableType(String fileName, Class<?> classObject, String methodName,
                                              String variableName) {
        try {
            fileName = fileName.replace('.', File.separatorChar) + ".class";
            ClassReader cr = new ClassReader(classObject.getClassLoader().getResourceAsStream(fileName));
            ClassNode cn = new ClassNode();
            cr.accept(cn, 0);

            for (MethodNode method : (List<MethodNode>) cn.methods) {
                if (method.name.equals(methodName)) {
                    for (LocalVariableNode var : method.localVariables) {
                        if (var.name.equals(variableName)) {
                            return var.desc;
                        }
                    }
                }
            }
            assertTrue(false, variableName + " not found within " + methodName + " method");
        } catch (Exception ignored) {}
        return "";
    }
}
