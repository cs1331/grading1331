package grading1331.builder;

import grading1331.ReflectAssert;
import grading1331.ReflectHelper;
import grading1331.gradescope.Visibility;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

/**
 * A class containing several factory methods used to generate a List of RunnableTest objects for
 * autograding the signatures of Class fields and methods.
 * <br>
 * The intended usage of the methods in this class is follows:
 * <ol>
 *     // todo update these instructions
 *   <li>An anonymous inner class that is an instance of {@link SignatureWrapper} is created with the precise signature
 *   of all the variables and methods to be searched for in the student class</li>
 *   <li>The name of the student class to test, as well as this anonymous inner class, are
 *   supplied to {@link #createBuilder(String)}</li>
 *   <li>One or more of the methods related to test weight distribution, such as {@link #allSeparateTests(double)},
 *   is invoked on the temporary builder object returned from (2), returning a List of RunnableTest objects</li>
 *   <li>This list of RunnableTest objects is returned as the result of {@link GeneratedTestSuite#getGeneratedTests()}
 *   in some test class that extends {@link GeneratedTestSuite}</li>
 * </ol>
 * @author Andrew Chafos
 */
public class SignatureTestBuilder {

    private final String studentClass;
    private final List<Runnable> currentTests;
    private final List<String> testNames;

    private SignatureTestBuilder(String studentClass) {
        this.studentClass = studentClass;
        this.currentTests = new ArrayList<>();
        this.testNames = new ArrayList<>();
    }

    public abstract static class SignatureWrapper {

    }

    public abstract static class ConstructorSignatureWrapper extends SignatureWrapper {

    }

    /**
     * Creates a new SignatureTestBuilder object based on the supplied student class name and an anonymous inner class
     * containing signatures to test
     * @param studentClass the name of the class to test
     * @return a builder object with fields matching the supplied parameters
     */
    public static SignatureTestBuilder createBuilder(String studentClass) {
        return new SignatureTestBuilder(studentClass);
    }

    private SignatureTestBuilder withSignatureTests(SignatureWrapper wrapper, boolean isStatic, boolean isAbstract, Map<String, String> typeConversionMap) {
        boolean isConstructor = wrapper instanceof ConstructorSignatureWrapper;
        List<Member> members;
        if (isConstructor) {
            members = Arrays.asList(wrapper.getClass().getDeclaredMethods());
        } else {
            members = Stream.concat(Arrays.stream(wrapper.getClass().getDeclaredFields()), Arrays.stream(wrapper.getClass().getDeclaredMethods())).collect(Collectors.toList());
        }
        members = members.stream().filter(m -> !m.getName().matches("this\\$\\d+")).collect(Collectors.toList());
        currentTests.addAll(members.stream().map(m -> getMemberRunnable(isConstructor, isStatic, isAbstract, m, typeConversionMap)).collect(Collectors.toList()));
        testNames.addAll(
            members.stream().map(
                member -> {
                    String name = "test " + member.getClass().getSimpleName() + " " + member.getName();
                    if (member instanceof Method) {
                        name += "(" + String.join(", ", getActualTypeNames(member.getName(), typeConversionMap, ((Method) member).getParameterTypes())) + ")";
                    }
                    String suffix = (member instanceof Method) ? " has correct signature and modifiers" : " has correct type and modifiers";
                    return name + suffix;
                }
            ).collect(Collectors.toList())
        );
        return this;
    }

    public SignatureTestBuilder withConstructorSignatureTests(ConstructorSignatureWrapper wrapper, Map<String, String> typeConversionMap) {
        return withSignatureTests(wrapper, false, false, typeConversionMap);
    }

    public SignatureTestBuilder withConstructorSignatureTests(ConstructorSignatureWrapper wrapper) {
        return withSignatureTests(wrapper, false, false, Map.of());
    }

    public SignatureTestBuilder withNonStaticSignatureTests(SignatureWrapper wrapper, Map<String, String> typeConversionMap) {
        return withSignatureTests(wrapper, false, false, typeConversionMap);
    }

    public SignatureTestBuilder withNonStaticSignatureTests(SignatureWrapper wrapper) {
        return withNonStaticSignatureTests(wrapper, Map.of());
    }

    public SignatureTestBuilder withStaticSignatureTests(SignatureWrapper wrapper, Map<String, String> typeConversionMap) {
        return withSignatureTests(wrapper, true, false, typeConversionMap);
    }

    public SignatureTestBuilder withStaticSignatureTests(SignatureWrapper wrapper) {
        return withStaticSignatureTests(wrapper, Map.of());
    }

    public SignatureTestBuilder withAbstractSignatureTests(SignatureWrapper wrapper, Map<String, String> typeConversionMap) {
        return withSignatureTests(wrapper, false, true, typeConversionMap);
    }

    public SignatureTestBuilder withAbstractSignatureTests(SignatureWrapper wrapper) {
        return withAbstractSignatureTests(wrapper, Map.of());
    }

    /**
     * Returns a List of RunnableTest objects in which each RunnableTest corresponds to a single field or method
     * whose signature is being tested, and each is given the passed-in test weight with VISIBLE visibility,
     * and the String appearing before each test name is "StudentClass - "
     * @param testWeight the weight to assign every single signature tests
     * @return a list of RunnableTest objects for every single field or method to be tested
     */
    public List<RunnableTest> allSeparateTests(double testWeight) {
        return allSeparateTests(testWeight, Visibility.VISIBLE);
    }

    /**
     * Returns a List of RunnableTest objects in which each RunnableTest corresponds to a single field or method
     * whose signature is being tested, and each is given the passed-in test weight and visibility,
     * and the String appearing before each test name is "StudentClass - "
     * @param testWeight the weight to assign every single signature tests
     * @param visibility the visibility to assign every single signature tests
     * @return a list of RunnableTest objects for every single field or method to be tested
     */
    public List<RunnableTest> allSeparateTests(double testWeight, Visibility visibility) {
        return allSeparateTests(studentClass + " - ", testWeight, visibility);
    }

    /**
     * Returns a List of RunnableTest objects in which each RunnableTest corresponds to a single field or method
     * whose signature is being tested, and each is given the passed-in test weight and visibility, with the supplied
     * prefix appearing before every test name
     * @param testWeight the weight to assign every single signature tests
     * @param visibility the visibility to assign every single signature tests
     * @return a list of RunnableTest objects for every single field or method to be tested
     */
    public List<RunnableTest> allSeparateTests(String prefix, double testWeight, Visibility visibility) {
        List<RunnableTest> runnableTests = new ArrayList<>();
        for (int i = 0;i < testNames.size(); i++) {
            runnableTests.add(new RunnableTest(prefix + testNames.get(i), "", testWeight, visibility, currentTests.get(i)));
        }
        return runnableTests;
    }

    public List<RunnableTest> allOneTest(double testWeight) {
        return allOneTest(studentClass + " - ", testWeight, Visibility.VISIBLE);
    }

    public List<RunnableTest> allOneTest(double testWeight, Visibility visibility) {
        return allOneTest(studentClass + " - ", testWeight, visibility);
    }

    public List<RunnableTest> allOneTest(String prefix, double testWeight, Visibility visibility) {
        Runnable combinedRunnable = () -> {
            for (Runnable runnable: currentTests) {
                runnable.run();
            }
        };
        String memberName;
        if (testNames.stream().allMatch(s -> s.contains("Field"))) {
            memberName = "variables";
        } else if (testNames.stream().allMatch(s -> s.contains("Method"))) {
            memberName = "methods";
        } else {
            memberName = "members";
        }
        return List.of(new RunnableTest(prefix + "test all " + memberName + " have correct signatures", "", testWeight, visibility, combinedRunnable));
    }

    private Runnable getMemberRunnable(boolean isConstructor, boolean isStatic, boolean isAbstract, Member member, Map<String, String> typeConversionMap) {
        return () -> {
            Member studentMember;
            if (isConstructor) {
                studentMember = ReflectHelper.getDeclaredConstructor(
                    studentClass, getActualType(member.getName(), typeConversionMap, ((Method) member).getParameterTypes())
                );
            } else {
                studentMember = (member instanceof Field) ? ReflectHelper.getDeclaredField(studentClass, member.getName()) :
                    ReflectHelper.getDeclaredMethod(
                        studentClass, member.getName(),
                        getActualType(member.getName(), typeConversionMap, ((Method) member).getParameterTypes())
                    );
            }
            if (Modifier.isPrivate(member.getModifiers())) {
                ReflectAssert.assertPrivate(studentMember);
            } else if (Modifier.isProtected(member.getModifiers())) {
                ReflectAssert.assertProtected(studentMember);
            } else if (Modifier.isPublic(member.getModifiers())) {
                ReflectAssert.assertPublic(studentMember);
            } else {
                ReflectAssert.assertPackageProtected(studentMember);
            }
            if (isStatic) {
                ReflectAssert.assertStatic(studentMember);
            } else {
                ReflectAssert.assertNotStatic(studentMember);
            }

            if (isAbstract) {
                ReflectAssert.assertAbstract(studentMember);
            } else {
                ReflectAssert.assertNotAbstract(studentMember);
            }

            if (Modifier.isFinal(member.getModifiers())) {
                ReflectAssert.assertFinal(studentMember);
            } else {
                ReflectAssert.assertNotFinal(studentMember);
            }

            if (!isConstructor) {
                if (member instanceof Method) {
                    Method expectedMethod = (Method) member;
                    String message = String.format("Your class should have a method named %s with return type %s", expectedMethod.getName(), expectedMethod.getReturnType().getSimpleName());
                    Class<?>[] expectedType = getActualType(member.getName(), typeConversionMap, expectedMethod.getReturnType());
                    assertEquals(expectedType[0], ((Method) studentMember).getReturnType(), message);
                } else {
                    Field expectedField = (Field) member;
                    Class<?> expectedType = getActualType(member.getName(), typeConversionMap, expectedField.getType())[0];
                    String message = String.format("Your class should have a field named %s of type %s", expectedField.getName(), expectedType.getSimpleName());
                    assertEquals(expectedType, ((Field) studentMember).getType(), message);
                }
            }
        };
    }

    private static Class<?>[] getActualType(String memberName, Map<String, String> typeConversionMap, Class<?>... originalTypes) {
        if (typeConversionMap.containsKey(memberName)) {
            Class<?>[] converted = new Class<?>[originalTypes.length];
            for (int i = 0;i < originalTypes.length; i++) {
                converted[i] = originalTypes[i].equals(Object.class) ? ReflectHelper.getClass(typeConversionMap.get(memberName)) : originalTypes[i];
            }
            return converted;
        } else {
            return originalTypes;
        }
    }

    private static List<String> getActualTypeNames(String memberName, Map<String, String> typeConversionMap, Class<?>... originalTypes) {
        List<String> typeNames = Arrays.stream(originalTypes).map(Class::getSimpleName).collect(Collectors.toList());
        if (typeConversionMap.containsKey(memberName)) {
            return typeNames.stream().map(name -> name.equals("Object") ? typeConversionMap.get(memberName) : name).collect(Collectors.toList());
        }
        return typeNames;
    }

}
