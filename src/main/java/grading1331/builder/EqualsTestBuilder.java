package grading1331.builder;

import grading1331.ConstructorHelper;
import grading1331.ReflectHelper;
import grading1331.gradescope.Visibility;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class EqualsTestBuilder {
    private final String studentClass;
    private final List<String> relevantAttributes;
    private final List<RunnableTest> currentTests;
    private final Map<String, String> fieldNameClassMap;
    private final List<Map<String, Object>> valueMaps;

    private EqualsTestBuilder(String studentClass, List<String> relevantAttributes, Map<String, String> fieldNameClassMap,
                              Map<String, Object> valueMap1, Map<String, Object> valueMap2) {
        this.studentClass = studentClass;
        this.currentTests = new ArrayList<>();
        this.relevantAttributes = relevantAttributes;
        this.fieldNameClassMap = fieldNameClassMap;
        this.valueMaps = List.of(valueMap1, valueMap2);
    }

    public static EqualsTestBuilder createBuilder(String studentClass, List<String> relevantAttributes, Map<String, String> fieldNameClassMap,
        Map<String, Object> valueMap1, Map<String, Object> valueMap2) {
        return new EqualsTestBuilder(studentClass, relevantAttributes, fieldNameClassMap, valueMap1, valueMap2);
    }

    private Method getEqualsMethod() {
        return ReflectHelper.getDeclaredMethod(studentClass, "equals", Object.class);
    }

    private void assertBasicChecks(ReflectHelper.MethodInvocationReport mir) {
        assertFalse(
            mir.exception.isPresent(), mir.exception.map(throwable -> "Method equals(Object other) threw an Exception: " + throwable.toString()).orElse("")
        );
        assertTrue(mir.returnValue.isPresent(), "equals(Object other) should have returned a value for this case!");
        assertEquals(boolean.class, getEqualsMethod().getReturnType(), "equals(Object other) should return a boolean!");
    }

    public EqualsTestBuilder withNullTest(String prefix, double testWeight, Visibility visibility) {
        Runnable nullTest = () -> {
            Object instance = ConstructorHelper.forceDefaultConstruction(ReflectHelper.getClass(studentClass));
            Method equalsMethod = getEqualsMethod();
            ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(equalsMethod, instance, (Object) null);
            assertBasicChecks(mir);
            assertEquals(false, mir.returnValue.get());
        };
        currentTests.add(new RunnableTest(prefix + "test equals(Object) handles null properly", "", testWeight, visibility, nullTest));
        return this;
    }

    public EqualsTestBuilder withNullTest(double testWeight, Visibility visibility) {
        return withNullTest(studentClass + " - ", testWeight, visibility);
    }

    public EqualsTestBuilder withNullTest(double testWeight) {
        return withNullTest(testWeight, Visibility.VISIBLE);
    }

    public EqualsTestBuilder withNonClassInputTest(String otherClassName, String prefix, double testWeight, Visibility visibility) {
        Runnable nonClassTest = () -> {
            Object instance = ConstructorHelper.forceDefaultConstruction(ReflectHelper.getClass(studentClass));
            Method equalsMethod = getEqualsMethod();
            Object param = ConstructorHelper.forceDefaultConstruction(ReflectHelper.getClass(otherClassName));
            ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(equalsMethod, instance, param);
            assertBasicChecks(mir);
            assertEquals(false, mir.returnValue.get());
        };
        currentTests.add(new RunnableTest(prefix + "test equals(Object) handles non-" + studentClass + " instance properly", "", testWeight, visibility, nonClassTest));
        return this;
    }

    public EqualsTestBuilder withNonClassInputTest(String otherClassName, double testWeight, Visibility visibility) {
        return withNonClassInputTest(otherClassName, studentClass + " - ", testWeight, visibility);
    }

    public EqualsTestBuilder withNonClassInputTest(String otherClassName, double testWeight) {
        return withNonClassInputTest(otherClassName, testWeight, Visibility.VISIBLE);
    }

    public EqualsTestBuilder withAttributeChecks(boolean allSeparate, String prefix, double testWeight, Visibility visibility) {
        List<Runnable> allTests = new ArrayList<>();
        List<String> fieldNames = new ArrayList<>(fieldNameClassMap.keySet());
        for (String fieldName: fieldNames) {
            Runnable attributeTest = () -> {
                for (int i = 0; i < 4; i++) {
                    Object instance1 = ConstructorHelper.forceDefaultConstruction(ReflectHelper.getClass(studentClass));
                    Object instance2 = ConstructorHelper.forceDefaultConstruction(ReflectHelper.getClass(studentClass));
                    int index1 = i % 2;
                    int index2 = i / 2;
                    for (String currentFieldName: fieldNames) {
                        Class<?> containingClass = ReflectHelper.getClass(fieldNameClassMap.get(currentFieldName));
                        if (currentFieldName.equals(fieldName)) {
                            ReflectHelper.setField(currentFieldName, containingClass, instance1, valueMaps.get(index1).get(currentFieldName));
                            ReflectHelper.setField(currentFieldName, containingClass, instance2, valueMaps.get(index2).get(currentFieldName));
                        } else {
                            ReflectHelper.setField(currentFieldName, containingClass, instance1, valueMaps.get(0).get(currentFieldName));
                            ReflectHelper.setField(currentFieldName, containingClass, instance2, valueMaps.get(0).get(currentFieldName));
                        }
                    }
                    Method equalsMethod = getEqualsMethod();
                    ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(equalsMethod, instance1, instance2);
                    assertBasicChecks(mir);
                    assertEquals(
                        !relevantAttributes.contains(fieldName) || index1 == index2, mir.returnValue.get(),
                        "equals(Object other) returned an incorrect value for this case!"
                    );
                    mir = ReflectHelper.invokeMethod(equalsMethod, instance2, instance1);
                    assertBasicChecks(mir);
                    assertEquals(
                        !relevantAttributes.contains(fieldName) || index1 == index2, mir.returnValue.get(),
                        "equals(Object other) returned an incorrect value for this case!"
                    );
                }
            };
            allTests.add(attributeTest);
        }
        if (allSeparate) {
            int fieldNum = 0;
            for (Runnable test: allTests) {
                currentTests.add(new RunnableTest(prefix + "test equals(Object) correct behavior for field " + fieldNum++, "", testWeight, visibility, test));
            }
        } else {
            currentTests.add(
                new RunnableTest(
                    prefix + "test equals(Object) correct behavior for all field combinations", "", testWeight, visibility,
                        () -> {
                            for (Runnable test: allTests) {
                                test.run();
                            }
                        }
                )
            );
        }
        return this;
    }

    public EqualsTestBuilder withAttributeChecks(boolean allSeparate, double testWeight, Visibility visibility) {
        return withAttributeChecks(allSeparate, studentClass + " - ", testWeight, visibility);
    }

    public EqualsTestBuilder withAttributeChecks(boolean allSeparate, double testWeight) {
        return withAttributeChecks(allSeparate, testWeight, Visibility.VISIBLE);
    }

    public List<RunnableTest> build() {
        return currentTests;
    }


}
