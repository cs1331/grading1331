package grading1331;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Random;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.*;

import grading1331.ReflectHelper;
import grading1331.ReflectAssert;

/**
 * Misc. functions which may be useful.
 *
 * @author jdierberger3, nzhong31
 * @version 1.0
 */
public final class Miscellaneous {

    /**
     * dnu.
     */
    private Miscellaneous() { }

    /**
     * A class which is not a superclass of any other class.
     *
     * @author jdierberger1
     */
    private static final class NotASuperclassOfAnything {
    }

    /**
     * Run through the basic tests for an equals method. This will test that an
     * instance is equal to itself, that it is not equal to null, and that it is not
     * equal to something which is not of the same type. If any of these tests fail,
     * the entire test will fail.
     *
     * @param message
     *            The failure message. Any occurrences of "%CAUSE" will be replaced
     *            with the test which failed, respectively "returned false for
     *            reference-equal argument", "got exception with null argument",
     *            "returned true for null argument", "got exception for incompatible
     *            type", or "returned true for incompatible type".
     * @param instance
     *            The instance to test with. A default one must be provided. If
     *            null, the test will always fail.
     * @param <T> The type to test
     */
    public static <T> void assertSimpleEqualsTests(String message, T instance) {
        if (instance == null) {
            fail("Instance to test equals on was null");
        }
        assertTrue(instance.equals(instance), message.replace("%CAUSE", "returned false for reference-equal argument"));
        try {
            assertFalse(instance.equals(null), message.replace("%CAUSE", "returned true for null argument"));
        } catch (NullPointerException npe) {
            fail(message.replace("%CAUSE", "got exception with null argument"));
        }
        try {
            assertFalse(instance.equals(new NotASuperclassOfAnything()),
                message.replace("%CAUSE", "returned true for incompatible type")
            );
        } catch (ClassCastException cce) {
            fail(message.replace("%CAUSE", "got exception for incompatible type"));
        }
    }

    /**
     * Test that the equals method does not fail with an exception when
     * reference-type fields are null in the callee or the argument. This method
     * will reflectively go through each reference type field in instance1 which is
     * not final and set the value of the field in both instances to null, failing
     * if a NullPointerException is caught.
     *
     * @param message The failure message.
     * @param instance1 The instance equals is being called on.
     * @param instance2 The instance being passed in.
     * @param <T> The type to test
     * @throws SecurityException If a field cannot be accessed.
     */
    public static <T> void assertEqualsHandlesNull(String message, T instance1, T instance2) {
        Field[] fields = instance1.getClass().getDeclaredFields();
        for (Field f : fields) {
            if (!f.getType().isPrimitive() && !Modifier.isFinal(f.getModifiers())) {
                f.setAccessible(true);
                try {
                    f.set(instance1, null);
                    f.set(instance2, null);
                    try {
                        instance1.equals(instance2);
                        instance2.equals(instance1);
                    } catch (NullPointerException npe) {
                        fail(message);
                    }
                } catch (IllegalAccessException e) {
                    throw new SecurityException(e);
                }
            }
        }
    }

    /**
    *   Calculates the Levenshtein distance between two strings
    *   (number of swaps to make 2 strings identical).
    *
    *   It then returns the number of swaps
    *
    *   @param expected the expected CharSequence
    *   @param submitted the actual CharSequence obtained from the student code
    *   @return the numerical character difference between the 2 Strings
    */
    public static int stringDistance(CharSequence expected, CharSequence submitted) {
        if (submitted == null || expected == null) {
            fail("Cannot calculate when passed a null string!");
        }

        int len0 = submitted.length() + 1;
        int len1 = expected.length() + 1;

        // the array of distances
        int[] cost = new int[len0];
        int[] newcost = new int[len0];

        String[] error = new String[len0];
        String[] newerror = new String[len0];

        String submittedUnderscore = ((String) submitted).replace(' ', '_');
        String expectedUnderscore = ((String) expected).replace(' ', '_');

        // initial cost of skipping prefix in String s0
        for (int i = 0; i < len0; i++) {
            cost[i] = i;
        }
        for (int i = 0; i < len0; i++) {
            if (i == 0) {
                error[i] = "";
            } else if (i == 1) {
                error[i] = "\nDelete first letter " + submitted.charAt(0);
            } else {
                error[i] = "\nDelete " + i + " letters to the front: '" + submittedUnderscore.subSequence(0, i) + "'.";
            }
        }

        // dynamically computing the array of distances

        // transformation cost for each letter in s1
        for (int j = 1; j < len1; j++) {
            // initial cost of skipping prefix in String s1
            newcost[0] = j;
            if (j == 1) {
                newerror[0] = "\nInsert first letter " + expected.charAt(0);
            } else if (j < expected.length()) {
                newerror[0] = "\nInsert letters " + expectedUnderscore.subSequence(0, j) + " to the beginning.";
            }
            // else newerror[i - 1] = "\nInsert first " + j + " letters";

            // transformation cost for each letter in s0
            for (int i = 1; i < len0; i++) {
                // matching current letters in both strings
                int match = (submitted.charAt(i - 1) == expected.charAt(j - 1)) ? 0 : 1;

                // computing cost for each transformation
                int costReplace = cost[i - 1] + match;
                int costInsert = cost[i] + 1;
                int costDelete = newcost[i - 1] + 1;

                // keep minimum cost
                newcost[i] = Math.min(Math.min(costInsert, costDelete), costReplace);

                // if a character is a space, let's make it an underline to show what we need
                // If this is confusing, we can just delete the turnary.
                char submittedChar = (submitted.charAt(i - 1) == ' ') ? '_' : submitted.charAt(i - 1);
                char expectedChar = (expected.charAt(j - 1) == ' ') ? '_' : expected.charAt(j - 1);

                // find out which operation we did - replace, insert, or delete and update newerror accordingly
                if (newcost[i] == costReplace) {
                    if (match == 1) {
                        newerror[i] = error[i - 1] + "\nReplace " + submittedChar
                            + " with " + expectedChar + " at index " + (j - 1);
                    } else {
                        newerror[i] = error[i - 1] + "";
                    }
                } else if (newcost[i] == costDelete) {
                    newerror[i] = (newerror[i - 1] + "\nDelete " + submittedChar + " at index " + (i - 1));
                } else {
                    newerror[i] = error[i] + "\nInsert " + expectedChar + " after index " + (j - 2);
                }
            }

            int[] swap = cost;
            cost = newcost;
            newcost = swap;
            String[] swaperror = error;
            error = newerror;
            newerror = swaperror;
        }
        return cost[len0 - 1];
    }


    /**
     * Uses stringDistance to provide a fuzzy matching version of .contains
     * @param expected the expected String object to check
     * @param got the actual String object to be compared against expected
     * @param distance the maximum permissible character difference between expected and got,
     *                 according to the Levenshtein distance formula
     * @return true if the expected String contains the actual String, within the accepted character
     * difference, and false otherwise
     */
    public static boolean fuzzyContains(String expected, String got, int distance) {
        if (expected.length() > got.length()) {
            return stringDistance(expected, got) <= distance;
        }

        for (int i = 0, j = expected.length(); j <= got.length(); i++, j++) {
            int dist = stringDistance(expected, got.substring(i, j));
            if (dist <= distance) {
                return true;
            }
        }
        return false;
    }

    /**
     * Marks the first difference between two strings by using a carat.
     * @param expected  the expected String.
     * @param submitted the String submitted by the student.
     * @return a String which contains spaces until the point at which the error was made, where the returned value will
     * terminate in a carat.
     */
    public static String markFirstDifference(CharSequence expected, CharSequence submitted) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < submitted.length(); i++) {
            if (i >= expected.length() || expected.charAt(i) != submitted.charAt(i)) {
                sb.append('^');
                return sb.toString();
            }

            sb.append(" ");
        }

        // Got to the end without finding a difference, so check string lengths
        if (expected.length() > submitted.length()) {
            sb.append('^');
        }
        return sb.toString();
    }

    /**
     * Asserts that the 2 input CharSequences are different by at most `distance` characters.  This
     * difference is determined by the stringDistance method
     * @param expected the CharSequence that is expected
     * @param actual the actual CharSequence that was captured to be compared against `expected`
     * @param distance the maximum character difference the sequences should have
     * @param printStrings When true, prints the expected vs. actual String.
     *                     When false, does not print any additional output.
     */
    public static void assertStringsMatch(CharSequence expected, CharSequence actual, int distance, boolean printStrings) {
        int calculatedDifference = stringDistance(expected, actual);
        String message = "The 2 compared Strings were different by a factor of " + calculatedDifference + " characters. ";
        message += "The acceptable distance is " + distance + " characters.";
        if (printStrings) {
            message += "\nHere is the expected String:\n" + expected + "\n";
            message += "Here is the String from your output:\n" + actual + "\n";
            message += markFirstDifference(expected, actual) + '\n';
        }
        if (calculatedDifference > distance) {
            /*
            Use fail directly instead of assertTrue so that the extra "expected true but got false" does not print
            See org.junit.jupiter.api.Assertions#assertTrue for more information. It is also a relatively
            simple wrapper around org.junit.jupiter.api.AssertionUtils#fail.
             */
            fail(message);
        }
    }

    /**
     * Disables Reflection Warnings so that the setMathDotRandom() method runs smoothly
     */
    @SuppressWarnings("unchecked")
    public static void disableAccessWarnings() {
        try {
            Class unsafeClass = Class.forName("sun.misc.Unsafe");
            Field field = unsafeClass.getDeclaredField("theUnsafe");
            field.setAccessible(true);
            Object unsafe = field.get(null);
            Method putObjectVolatile = unsafeClass.getDeclaredMethod("putObjectVolatile", Object.class, long.class, Object.class);
            Method staticFieldOffset = unsafeClass.getDeclaredMethod("staticFieldOffset", Field.class);
            Class loggerClass = Class.forName("jdk.internal.module.IllegalAccessLogger");
            Field loggerField = loggerClass.getDeclaredField("logger");
            Long offset = (Long) staticFieldOffset.invoke(unsafe, loggerField);
            putObjectVolatile.invoke(unsafe, loggerClass, offset, null);
        } catch (Exception ignored) {
        }
    }

    private static int RANDOM_CHANGES = 0;

    /**
     * Modifies the JVM such that Math.random() invokes the nextDouble() method
     * of the supplied Random subclass rather than java.util.Random
     * @param randomSubClassObj the instance of some subclass of Random we want Math.random() to use
     * @throws IllegalAccessException if the Random class Math.random() references cannot be changed via Reflection
     */
    public static void setMathDotRandom(Random randomSubClassObj) throws IllegalAccessException {
        disableAccessWarnings();
        try {
            Field mf = Field.class.getDeclaredField("modifiers");
            mf.setAccessible(true);
            Field randomField = Math.class.getDeclaredClasses()[0].getDeclaredFields()[0];
            randomField.setAccessible(true);
            mf.setInt(randomField, randomField.getModifiers() & ~Modifier.FINAL);
            randomField.set(null, randomSubClassObj);
            assertSame(
                randomSubClassObj, randomField.get(null),
                "Could not properly change Math.random() to passed-in Random instance after " + RANDOM_CHANGES + " invocations of this method!"
            );
            RANDOM_CHANGES++;
        } catch (Exception someException) {
            throw new IllegalAccessException(someException.getMessage());
        }
    }

    /**
     * Modifies the JVM such that Math.random() invokes the nextDouble() method
     * based on a Random subclass that uses the given mapping to decide the value of nextDouble()
     * @param intToDoubleMapping a Function that maps ints to some double between 0 and 1, where the input integer
     *                           is a number between 0 and Integer.MAX_VALUE representing how many times
     *                           Math.random() has been invoked
     * @throws IllegalAccessException if the Random class Math.random() references cannot be changed via Reflection
     */
    public static void setMathDotRandom(Function<Integer, Double> intToDoubleMapping) throws IllegalAccessException {
        setMathDotRandom(new FunctionBasedRandom(intToDoubleMapping));
    }

    /**
     * Gets and returns the toString output for an instance of a given class.
     * @param cls Class of the instance passed in
     * @param instance The instance to test with
     * @return Output of the toString method of that instance
     */
    public static String getToStringOutput(Class<?> cls, Object instance) {
        Method toString = ReflectHelper.getDeclaredMethod(cls, "toString");
        assertEquals(String.class, toString.getReturnType(), "toString() should return a String");
        ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(toString, instance);
        assertFalse(mir.exception.isPresent(), "toString() threw an exception: " + (mir.exception.isPresent() ? mir.exception.get().toString() : ""));
        assertTrue(mir.returnValue.isPresent(), "toString() should have returned a String");
        return mir.returnValue.get().toString().replaceAll("\\s+", " ").trim();
    }

    public static class FunctionBasedRandom extends Random {
        private int numInvocations = 0;
        private final Function<Integer, Double> invocationMapping;

        public FunctionBasedRandom(Function<Integer, Double> invocationMapping) {
            this.invocationMapping = invocationMapping;
        }

        public double nextDouble() {
            return invocationMapping.apply(numInvocations++);
        }
    }


}
