package testutil;

import java.util.Random;
import java.util.Scanner;

public class ExampleArbitraryClass {

    public ExampleArbitraryClass(String a) {
        
    }

    ExampleArbitraryClass() {
        
    }

    public final int zero = 0;

    public int settable = 1;

    int test = 2;

    public static String a = "a";

    static String b = "b";

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println(args[0]);
        System.out.println(sc.nextInt());
        sc.close();
    }

    public static void print() {
        System.out.println("Hello, world!!");
    }

    void m() {
        ((Object) null).toString();
    }

    @CustomAnnotation(foo="bar")
    public int timesByRandom(Random r, Integer toMul) {
        return toMul * r.nextInt();
    }

    @Override
    @CustomAnnotation(foo="bar")
    public boolean equals(Object other) {
        return this == other;
    }

    private String getterSetterCheck;
    private String wrongGetterSetter;

    public String getGetterSetterCheck() {
        return getterSetterCheck;
    }

    public String getWrongGetterSetter() {
        return "hahahaha I am fake and wrong";
    }

    public void setGetterSetterCheck(String s) {
        getterSetterCheck = s;
    }

    public String setWrongGetterSetter(String s) {
        wrongGetterSetter = "hahahaha I am fake and wrong";
        return wrongGetterSetter;
    }

    public int setTest(int i) {
        test = i;
        return i;
    }

}
