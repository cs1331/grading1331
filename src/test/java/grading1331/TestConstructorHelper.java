package grading1331;

import grading1331.gradescope.GradedTestRunner;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;

import java.lang.reflect.Array;
import java.lang.reflect.Field;

import static org.junit.jupiter.api.Assertions.*;

public class TestConstructorHelper {
    @Test
    public void testAllBasicTypes() {
        Object instance = ConstructorHelper.forceDefaultConstruction(AllBasicTypes.class);
        for (Field field: AllBasicTypes.class.getDeclaredFields()) {
            if (field.getName().equals("s")) {
                assertEquals("", ReflectHelper.getFieldValue("s", AllBasicTypes.class, instance));
            } else {
                assertEquals(ConstructorHelper.defaultValueMap.get(field.getType()), ReflectHelper.getFieldValue(field.getName(), AllBasicTypes.class, instance));
            }
        }
    }

    @Test
    public void testAllPrivateConstructors() {
        Object a = ConstructorHelper.forceDefaultConstruction(ClassWithNoConstructors.class);
        assertNotNull(a);
    }

    @Test
    public void testOnlyDefaultConstructor() {
        Object a = ConstructorHelper.forceDefaultConstruction(ClassWithOnlyDefaultConstructor.class);
        assertNotNull(a);
        assertEquals(0, ReflectHelper.getFieldValue("i", ClassWithOnlyDefaultConstructor.class, a));
    }

    @Test
    public void testSelfCircularNullParamsAllowed() {
        Object instance = ConstructorHelper.forceDefaultConstruction(SelfCircular.class);
        assertNotNull(instance);
        assertNotNull(ReflectHelper.getFieldValue("a", SelfCircular.class, instance));
        assertNull(ReflectHelper.getFieldValue("a", SelfCircular.class, ReflectHelper.getFieldValue("a", SelfCircular.class, instance)));
    }

    @Test
    public void testSelfCircularNullParamsNotAllowed() {
        assertThrows(AssertionError.class, () -> ConstructorHelper.forceDefaultConstruction(SelfCircular.class, 0, true));
    }

    @Test
    public void testOtherCircularNullParamsAllowed() {
        Object instance = ConstructorHelper.forceDefaultConstruction(OtherCircular1.class);
        assertNotNull(instance);
        Object bInstance = ReflectHelper.getFieldValue("b", OtherCircular1.class, instance);
        assertNotNull(bInstance);
        Object aInstanceOfBInstance = ReflectHelper.getFieldValue("a", OtherCircular2.class, bInstance);
        assertNull(aInstanceOfBInstance);
    }

    @Test
    public void testAllConstructorsThrowExceptions() {
        try {
            ConstructorHelper.forceDefaultConstruction(AllConstructorsThrowExceptions.class);
            fail("Should not make it to this line!");
        } catch (Throwable t) {
            assertEquals(AssertionFailedError.class, t.getClass());
            assertEquals(
                "Could not create an instance of class AllConstructorsThrowExceptions"
                + " without every constructor throwing an exception, a circular relationship occurring, or an invalid enum being supplied when it needs to be non-null",
                t.getMessage()
            );
        }
    }

    @Test
    public void testSomeConstructorsThrowExceptions() {
        Object instance = ConstructorHelper.forceDefaultConstruction(SomeConstructorsThrowExceptions.class);
        assertEquals("", ReflectHelper.getFieldValue("s", SomeConstructorsThrowExceptions.class, instance));
        assertEquals(0, ReflectHelper.getFieldValue("i", SomeConstructorsThrowExceptions.class, instance));
    }

    @Test
    public void testConstructorWithArrayTypes() {
        Object instance = ConstructorHelper.forceDefaultConstruction(ClassWithArrays.class);
        Object intArr = ReflectHelper.getFieldValue("intArr", ClassWithArrays.class, instance);
        assertNotNull(intArr);
        assertTrue(intArr.getClass().isArray());
        assertEquals(int[][].class, intArr.getClass().getComponentType());
        int[][][] casted = (int[][][]) intArr;
        assertArrayEquals(new int[0][0][0], casted);
        Object sArr = ReflectHelper.getFieldValue("sArr", ClassWithArrays.class, instance);
        assertNotNull(sArr);
        assertTrue(sArr.getClass().isArray());
        assertEquals(String.class, sArr.getClass().getComponentType());
        String[] casted2 = (String[]) sArr;
        assertArrayEquals(new String[0], casted2);
    }

    @Test
    public void testConstructorWithEnumTypes() {
        Object instance = ConstructorHelper.forceDefaultConstruction(ClassWithEnumTypes.class);
        assertNotNull(instance);
        assertEquals(ClassWithEnumTypes.Enum2.HELLO, ReflectHelper.getFieldValue("enum2", ClassWithEnumTypes.class, instance));
        assertNull(ReflectHelper.getFieldValue("enum1", ClassWithEnumTypes.class, instance));
        try {
            ConstructorHelper.forceDefaultConstruction(ClassWithEnumTypes.class, 0, true);
            fail("The above method call should fail");
        } catch (Throwable t) {
            assertEquals(AssertionFailedError.class, t.getClass());
            assertEquals(
                "Could not create an instance of class ClassWithEnumTypes"
                 + " without every constructor throwing an exception, a circular relationship occurring, or an invalid enum being supplied when it needs to be non-null",
                t.getMessage()
            );
        }
    }

    @Test
    public void testConstructorInterfaceParam() {
        Object instance = ConstructorHelper.forceDefaultConstruction(ClassWithInterfaceConstructor.class);
        Object comparableInstance = ReflectHelper.getFieldValue("myComparable", ClassWithInterfaceConstructor.class, instance);
        assertNotNull(comparableInstance);
        ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(comparableInstance.getClass(), "compareTo", comparableInstance, (Object) null);
        assertFalse(mir.exception.isPresent());
        assertTrue(mir.returnValue.isPresent());
        assertEquals(0, mir.returnValue.get());
    }

    @Test
    public void testConstructorAbstractClassParam() {
        Object instance = ConstructorHelper.forceDefaultConstruction(ClassWithAbstractClassConstructor.class, 0, true);
        assertNotNull(instance);
        Object abstractClassInstance = ReflectHelper.getFieldValue("myTestClass", ClassWithAbstractClassConstructor.class, instance);
        assertNotNull(abstractClassInstance);
        ReflectHelper.MethodInvocationReport mir = ReflectHelper.invokeMethod(abstractClassInstance.getClass(), "myMethod", abstractClassInstance);
        assertFalse(mir.exception.isPresent(), mir.exception.map(GradedTestRunner::getStackTrace).orElse(""));
        assertTrue(mir.returnValue.isPresent());
        assertEquals(0, mir.returnValue.get());
    }

    @Test
    public void testConstructorWithCopyAndOthers() {
        Object instance = ConstructorHelper.forceDefaultConstruction(ClassWithMultipleConstructorsAndCopy.class, 4);
        Object doubleArr = ReflectHelper.getFieldValue("doubleArr", ClassWithMultipleConstructorsAndCopy.class, instance);
        assertNotNull(doubleArr);
        assertTrue(doubleArr.getClass().isArray());
        assertEquals(4, Array.getLength(doubleArr));
    }

    // todo add more tests for the new methods designed to test constructors
}

class AllBasicTypes {
    public String s;
    public Byte wrapperB;
    public short myShort;
    public Short myWrapperShort;
    public int i;
    public Integer wrapperI;
    public long l;
    public Long wrapperL;
    public double d;
    public Double wrapperD;
    public float f;
    public Float wrapperF;
    public char c;
    public Character wrapperC;
    public boolean bool;
    public Boolean wrapperBool;

    public AllBasicTypes(String s, Byte wrapperB, short myShort, Short myWrapperShort, int i, Integer wrapperI, long l,
                         Long wrapperL, double d, Double wrapperD, float f, Float wrapperF, char c, Character wrapperC,
                         boolean bool, Boolean wrapperBool) {
        this.s = s;
        this.wrapperB = wrapperB;
        this.myShort = myShort;
        this.myWrapperShort = myWrapperShort;
        this.i = i;
        this.wrapperI = wrapperI;
        this.l = l;
        this.wrapperL = wrapperL;
        this.d = d;
        this.wrapperD = wrapperD;
        this.f = f;
        this.wrapperF = wrapperF;
        this.c = c;
        this.wrapperC = wrapperC;
        this.bool = bool;
        this.wrapperBool = wrapperBool;
    }
}

class ClassWithNoConstructors {
    private ClassWithNoConstructors() {}
}

class ClassWithOnlyDefaultConstructor {
    int i;
    ClassWithOnlyDefaultConstructor(int i) {
        this.i = i;
    }
}

class SelfCircular {
    public SelfCircular a;
    public SelfCircular(SelfCircular a) {
        this.a = a;
    }
}

class OtherCircular1 {
    public OtherCircular2 b;
    public OtherCircular1(OtherCircular2 b) {
        this.b = b;
    }
}

class OtherCircular2 {
    public OtherCircular1 a;
    public OtherCircular2(OtherCircular1 a) {
        this.a = a;
    }
}

class AllConstructorsThrowExceptions {
    public AllConstructorsThrowExceptions() {
        throw new StackOverflowError();
    }

    public AllConstructorsThrowExceptions(String s) {
        throw new IllegalStateException();
    }
}

class SomeConstructorsThrowExceptions {
    public String s;
    public int i;
    public SomeConstructorsThrowExceptions() {
        throw new StackOverflowError();
    }

    public SomeConstructorsThrowExceptions(String s) {
        this.s = s;
        throw new IllegalStateException();
    }

    public SomeConstructorsThrowExceptions(String s, int i) {
        this.s = s;
        this.i = i;
    }
}

class ClassWithArrays {
    public final int[][][] intArr;
    public final String[] sArr;
    public ClassWithArrays(int[][][] intArr, String[] sArr) {
        this.intArr = intArr;
        this.sArr = sArr;
    }
}

class ClassWithEnumTypes {
    enum Enum1 {

    }
    enum Enum2 {
        HELLO, OK, FINE
    }
    public Enum1 enum1;
    public Enum2 enum2;

    public ClassWithEnumTypes(Enum1 enum1, Enum2 enum2) {
        this.enum1 = enum1;
        this.enum2 = enum2;
    }
}

class ClassWithInterfaceConstructor {
    private final Comparable<String> myComparable;

    public ClassWithInterfaceConstructor(Comparable<String> myComparable) {
        this.myComparable = myComparable;
    }
}

abstract class TestClass {
    public int myMethod() {
        return 0;
    }
}

class ClassWithAbstractClassConstructor {

    public TestClass myTestClass;

    public ClassWithAbstractClassConstructor(TestClass myTestClass) {
        this.myTestClass = myTestClass;
    }
}


class ClassWithMultipleConstructorsAndCopy {
    private String someStr;
    private int int1;
    private int int2;
    private double[] doubleArr;

    public ClassWithMultipleConstructorsAndCopy(ClassWithMultipleConstructorsAndCopy other) {
        this.someStr = other.someStr;
        this.int1 = other.int1;
        this.int2 = other.int2;
        this.doubleArr = new double[other.doubleArr.length];
        for (int i = 0; i < this.doubleArr.length; i++) {
            this.doubleArr[i] = other.doubleArr[i];
        }
    }

    public ClassWithMultipleConstructorsAndCopy(String someStr, double[] doubleArr) {
        this(someStr, 0, doubleArr);
    }

    public ClassWithMultipleConstructorsAndCopy(String someStr, int int1, double[] doubleArr) {
        this(someStr, int1, int1 + 3000, doubleArr);
    }

    public ClassWithMultipleConstructorsAndCopy(String someStr, int int1, int int2, double[] doubleArr) {
        this.someStr = someStr;
        this.int1 = int1;
        this.int2 = int2;
        this.doubleArr = doubleArr;
        for (int i = 0; i < 4; i++) {
            if (this.doubleArr[i] < 0.00 || this.doubleArr[i] > 1.00) {
                this.doubleArr[i] = 1.00;
            }
        }
    }
}