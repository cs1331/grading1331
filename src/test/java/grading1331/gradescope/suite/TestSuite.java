package grading1331.gradescope.suite;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.fail;

import grading1331.gradescope.GradedTest;
import grading1331.gradescope.GradedTestRunner;
import grading1331.gradescope.Visibility;
import org.junit.jupiter.api.extension.ExtendWith;

public class TestSuite {
    @GradedTest(name = "This Test should pass", max_score = 5.0)
    public void thisShouldPass() {
        assertTrue(true);
        System.out.println("Happy yay!");
    }

    @GradedTest(name = "This Test should fail", max_score = 5.0)
    public void thisShouldFail() {
        fail("Here is a message for the student!");
    }

    @GradedTest(name = "This should be a hidden test", max_score = 10.0, visibility = Visibility.HIDDEN)
    public void hiddenTest() {
        fail("Here is a message for the TAs!");
    }
}
