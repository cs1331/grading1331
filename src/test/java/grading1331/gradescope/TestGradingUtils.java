package grading1331.gradescope;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.*;
import grading1331.gradescope.suite.RigorousTestSuite;
import grading1331.gradescope.suite.SuiteWithSlowTests;
import grading1331.gradescope.suite.TestSuite;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

public class TestGradingUtils {

    @BeforeEach
    public void resetCheckstyleConfig() {
        GradingUtils.setGraderConfigOptions(
            List.of(), false, false, 0, 0, 0,
            List.of(), List.of(), List.of(), true, false
        );
    }

    @Test
    public void testFileThatShouldCompile() {
        File compilingFile = new File("src/test/java/testutil/ExampleArbitraryClass.java");
        assertEquals(List.of(), GradingUtils.getCompilerErrors(compilingFile));
        assertTrue(GradingUtils.fileCompiles(compilingFile));
        assertTrue(GradingUtils.getCompilerErrors(compilingFile).isEmpty());
    }

    private void writeToTempFile(File tempFile, String fileContents) throws IOException {
        tempFile.deleteOnExit();
        BufferedWriter writer = new BufferedWriter(new FileWriter(tempFile));
        writer.write(fileContents);
        writer.flush();
    }

    @Test
    public void testFileThatShouldNotCompile() throws IOException {
        File nonCompilingFile = new File("NonCompiling.java");
        writeToTempFile(nonCompilingFile, "public class NonCompiling {");
        assertFalse(GradingUtils.fileCompiles(nonCompilingFile));
        assertFalse(GradingUtils.getCompilerErrors(nonCompilingFile).isEmpty());
    }

    /**
     * GradedTestRunner tests, including checks to see if JSON output is formatted correctly
     * and to see if Checkstyle tests work as expected
     */

    private static final HashMap<String, Object[]> testMapping;

    static {
        testMapping = new HashMap<>();
        testMapping.put("This Test should pass", new Object[]{"", 5.0, 5.0, "visible", "Happy yay!"});
        String visibleError = "Test Failed!\norg.opentest4j.AssertionFailedError: Here is a message for the student!";
        testMapping.put("This Test should fail", new Object[]{"", 0.0, 5.0, "visible", visibleError});
        String hiddenError = "Test Failed!\norg.opentest4j.AssertionFailedError: Here is a message for the TAs!";
        testMapping.put("This should be a hidden test", new Object[]{"", 0.0, 10.0, "hidden", hiddenError});
        testMapping.put("Illegal Imports", new Object[]{"", 0.0, 0.0, "visible", ""});
        testMapping.put("Illegal Usages", new Object[]{"", 0.0, 0.0, "visible", ""});
    }

    private void assertTestNodeCorrect(ObjectNode testNode) {
        assertEquals(6, testNode.size());
        assertEquals(TextNode.class, testNode.get("name").getClass());
        assertTrue(testMapping.keySet().contains(testNode.get("name").textValue()));
        Object[] params = testMapping.get(testNode.get("name").textValue());
        assertEquals(TextNode.class, testNode.get("number").getClass());
        assertEquals(params[0], testNode.get("number").textValue());
        assertEquals(DoubleNode.class, testNode.get("score").getClass());
        assertEquals((Double) params[1], testNode.get("score").doubleValue(), .00001);
        assertEquals(DoubleNode.class, testNode.get("max_score").getClass());
        assertEquals((Double) params[2], testNode.get("max_score").doubleValue(), .00001);
        assertEquals(TextNode.class, testNode.get("visibility").getClass());
        assertEquals(params[3], testNode.get("visibility").textValue());
        assertEquals(TextNode.class, testNode.get("output").getClass());
        if (!testNode.get("output").textValue().contains(params[4].toString())) {
            fail(testNode.get("output").textValue());
        }
    }

    private ArrayNode testBasicSuiteWorks(Class<?> testSuite, int numExpectedTests) throws JsonProcessingException {
        Class<?>[] classes = {testSuite};
        String jsonOutput = GradingUtils.getOutputFromAllTests(classes).toPrettyString();
        assertNotEquals("", jsonOutput.replaceAll("\\s+", ""));
        ObjectNode actualJSON = (ObjectNode) new ObjectMapper().readTree(jsonOutput);
        assertNotNull(actualJSON.get("execution_time"));
        assertNotNull(actualJSON.get("tests"));
        assertEquals(IntNode.class, actualJSON.findValue("execution_time").getClass());
        assertEquals(ArrayNode.class, actualJSON.findValue("tests").getClass());
        assertTrue(2 == actualJSON.size() || 3 == actualJSON.size());
        ArrayNode testsNode = (ArrayNode) actualJSON.findValue("tests");
        assertEquals(numExpectedTests, testsNode.size());
        return testsNode;
    }

    private ArrayNode testBasicSuiteWorks(int numExpectedTests) throws JsonProcessingException {
        return testBasicSuiteWorks(TestSuite.class, numExpectedTests);
    }

    @Test
    public void testGradingSuiteBasic() throws JsonProcessingException {
        GradingUtils.setGraderConfigOptions(
            new ArrayList<>(), false, false, 0, 0,
            0, new ArrayList<>(),  new ArrayList<>(), new ArrayList<>(), true, false
        );
        ArrayNode testsNode = testBasicSuiteWorks(5);
        for (int i = 0; i < 5; i++) {
            assertEquals(ObjectNode.class, testsNode.get(i).getClass());
            assertTestNodeCorrect((ObjectNode) testsNode.get(i));
        }
    }

    private void assertSpecificTestNodeReceivesScore(String testName, double expectedScore) {
        ArrayNode testsNode;
        try {
            testsNode = testBasicSuiteWorks("Checkstyle".equals(testName) ? 6 : 5);
            ObjectNode testNode = null;
            for (int i = 0; i < 6; i++) {
                assertEquals(ObjectNode.class, testsNode.get(i).getClass());
                ObjectNode potentialCheckstyle = (ObjectNode) testsNode.get(i);
                if (testName.equals(potentialCheckstyle.get("name").textValue())) {
                    testNode = potentialCheckstyle;
                    break;
                }
            }
            assertNotNull(testNode);
            assertEquals(expectedScore, testNode.get("score").asDouble(), .00001);
        } catch (JsonProcessingException jpe) {
            fail("Got JsonProcessingException");
        }
    }

    private void testCheckstyleOptions(boolean checkstyleEnabled, boolean javadocsEnabled,
                                       int checkstyleCap, double expectedScore, List<String> ignoredErrors,
                                       String fileName) {
        String sampleClassPath = "src/test/java/grading1331/gradescope/suite/" + fileName;
        assertTrue(new File(sampleClassPath).exists());
        GradingUtils.setGraderConfigOptions(
            List.of(sampleClassPath), checkstyleEnabled, javadocsEnabled, checkstyleCap, 0,
            0, new ArrayList<>(),  new ArrayList<>(), ignoredErrors, true, false
        );
        assertSpecificTestNodeReceivesScore("Checkstyle", expectedScore);
    }

    private void testCheckstyleOptions(boolean checkstyleEnabled, boolean javadocsEnabled, int checkstyleCap,
                                       double expectedScore, List<String> ignoredErrors) {
        testCheckstyleOptions(
                checkstyleEnabled, javadocsEnabled, checkstyleCap, expectedScore, ignoredErrors,
                "SampleClass.java");
    }

    private void testCheckstyleOptions(boolean checkstyleEnabled, boolean javadocsEnabled, int checkstyleCap,
                                       double expectedScore) {
        testCheckstyleOptions(checkstyleEnabled, javadocsEnabled, checkstyleCap, expectedScore, new ArrayList<>());
    }

    private void testIllegalImports(List<String> allowedImports, int deductionWeight, int expectedScore) {
        String sampleClassPath = "src/test/java/grading1331/gradescope/suite/ClassWithIllegalImports.java";
        assertTrue(new File(sampleClassPath).exists());
        GradingUtils.setGraderConfigOptions(
            List.of(sampleClassPath), false, false, 0, deductionWeight,
            0, allowedImports,  new ArrayList<>(), new ArrayList<>(), true, false
        );
        assertSpecificTestNodeReceivesScore("Illegal Imports", expectedScore);
    }

    private void testIllegalUsages(List<String> illegalUsages, int deductionWeight, int expectedScore) {
        String sampleClassPath = "src/test/java/grading1331/gradescope/suite/ClassWithBannedUsages.java";
        assertTrue(new File(sampleClassPath).exists());
        GradingUtils.setGraderConfigOptions(
            List.of(sampleClassPath), false, false, 0, 0,
            deductionWeight, new ArrayList<>(),  illegalUsages, new ArrayList<>(), true, false
        );
        assertSpecificTestNodeReceivesScore("Illegal Usages", expectedScore);
    }

    @Test
    public void testGradingSuiteCheckstyleAllOptionsArbitraryCap() {
        testCheckstyleOptions(true, true, 100, -17);
    }

    @Test
    public void testGradingSuiteCheckstyleAllOptionsLowCap() {
        testCheckstyleOptions(true, true, 8, -8);
    }

    @Test
    public void testGradingSuiteCheckstyleAllOptionsZeroCap() {
        testCheckstyleOptions(true, true, 0, 0);
    }

    @Test
    public void testGradingSuiteCheckstyleOnlyCheckstyle() {
        testCheckstyleOptions(true, false, 100, -15);
    }

    @Test
    public void testGradingSuiteCheckstyleOnlyJavadocs() {
        testCheckstyleOptions(false, true, 100, -2);
    }

    @Test
    public void testGradingSuiteCheckstylePerfectSubmission() {
        File temp = new File("src/test/java/grading1331/gradescope/suite/A.java");
        try {
            boolean successful = temp.createNewFile();
            assertTrue(successful);
            PrintWriter writer = new PrintWriter(new FileWriter(temp));
            writer.println("public class A {");
            writer.println("    public static void main(String[] args) {");
            writer.println("        System.out.println('a');");
            writer.println("    }");
            writer.println("}");
            writer.flush();
            testCheckstyleOptions(
                true, false, 37, 0.0, List.of(), "A.java"
            );
        } catch (IOException ioe) {
            fail(ioe.toString());
        } finally {
            temp.deleteOnExit();
        }
    }

    @Test
    public void testIllegalImportsNothingAllowed() {
        testIllegalImports(new ArrayList<>(), 8, -32);
    }

    @Test
    public void testIllegalImportsAllowPackage() {
        testIllegalImports(List.of("java.util"), 8, -8);
    }

    @Test
    public void testIllegalImportsAllowClass() {
        testIllegalImports(List.of("java.io.IOException"), 8, -24);
    }

    @Test
    public void testIllegalUsagesAnythingGoes() {
        testIllegalUsages(new ArrayList<>(), 8, 0);
    }

    @Test
    public void testIllegalUsagesDisallowVar() {
        testIllegalUsages(List.of("(^|;|\\s+)var\\s+"), 8, -16);
    }

    @Test
    public void testIllegalUsagesDisallowArrayCopy() {
        testIllegalUsages(List.of("arraycopy"), 8, -8);
    }

    @Test
    public void testIllegalUsagesDisallowStandard() {
        testIllegalUsages(List.of("(^|;|\\s+)var\\s+", "arraycopy", "System\\s*\\.\\s*exit"), 8, -32);
    }

    private void testNonNegative(boolean forceNonNegative) throws JsonProcessingException {
        Class<?>[] classes = {TestSuite.class};
        String sampleClassPath = "src/test/java/grading1331/gradescope/suite/SampleClass.java";
        GradingUtils.setGraderConfigOptions(
            List.of(sampleClassPath), true, false, 100, 0,
            0, new ArrayList<>(),  new ArrayList<>(), new ArrayList<>(), forceNonNegative, false
        );
        String jsonOutput = GradingUtils.getOutputFromAllTests(classes).toPrettyString();
        ObjectNode actualJSON = (ObjectNode) new ObjectMapper().readTree(jsonOutput);
        ArrayNode testsNode = (ArrayNode) actualJSON.findValue("tests");
        assertTrue(new File(sampleClassPath).exists());
        double sum = 0;
        for (int i = 0; i < 5; i++) {
            assertEquals(ObjectNode.class, testsNode.get(i).getClass());
            ObjectNode curr = (ObjectNode) testsNode.get(i);
            sum += curr.get("score").asDouble();
        }
        assertTrue(sum < 0.0);
        assertNotNull(actualJSON.get("score"));
        if (forceNonNegative) {
            assertEquals(actualJSON.get("score").asDouble(), 0.0, 0.0001);
        } else {
            assertEquals(sum, actualJSON.get("score").asDouble(), 0.0001);
        }
    }

    @Test
    public void assertTotalScoreNotNegative() throws JsonProcessingException {
        testNonNegative(true);
    }

    @Test
    public void testForceNonNegativeFalse() throws JsonProcessingException {
        testNonNegative(false);
    }

    @Test
    public void testIgnoreIndentation() {
        testCheckstyleOptions(true, true, 100, -14, List.of("[Indentation]"));
    }

    @Test
    public void testHashCodeAndCovariantEqualsEnabled() {
        testCheckstyleOptions(true, true,
            100, -3, new ArrayList<>(), "ClassWithBadEquals.java"
        );
    }

    @Test
    public void testIgnoreCovariantEquals() {
        testCheckstyleOptions(true, true,
            100, -2, List.of("[CovariantEquals]"), "ClassWithBadEquals.java"
        );
    }

    @Test
    public void testIgnoreHashcode() {
        testCheckstyleOptions(true, true,
            100, -2, List.of("[EqualsHashCode]"), "ClassWithBadEquals.java"
        );
    }

    @Test
    public void testIgnoreHashCodeAndCovariantEquals() {
        testCheckstyleOptions(true, true,
            100, -1, List.of("[CovariantEquals]", "[EqualsHashCode]"),
            "ClassWithBadEquals.java"
        );
    }

    @Test
    public void testTestSuiteWithBeforeEachWhenClassDoesNotExist() {
        ArrayNode testsNode = null;
        try {
            testsNode = testBasicSuiteWorks(RigorousTestSuite.class, 4);
        } catch (Throwable t) {
            fail(t.toString());
        }
        Map<String, ObjectNode> testMap = new HashMap<>();
        for (int i = 0; i < 4; i++) {
            ObjectNode currTest = (ObjectNode) testsNode.get(i);
            testMap.put(currTest.get("name").asText(), currTest);
        }
        System.out.println(testsNode.toPrettyString());
        assertEquals(0.0, testMap.get("Only passes when Class is found").get("score").asDouble());
        assertEquals(0.0, testMap.get("Should always fail").get("score").asDouble());
    }

    //@Test
    public void testTestSuiteWorksSlowTest() {
        ArrayNode testsNode = null;
        try {
            testsNode = testBasicSuiteWorks(SuiteWithSlowTests.class, 5);
        } catch (Throwable t) {
            fail(t.toString());
        }
        Map<String, ObjectNode> testMap = new HashMap<>();
        for (int i = 0; i < 5; i++) {
            ObjectNode currTest = (ObjectNode) testsNode.get(i);
            testMap.put(currTest.get("name").asText(), currTest);
        }
        assertEquals(0.0, testMap.get("Really slow test").get("score").asDouble());
        assertEquals(0.0, testMap.get("Infinite Loop").get("score").asDouble());
        assertTrue(testMap.get("Really slow test").get("output").asText().startsWith("Test Failed!\njava.util.concurrent.TimeoutException"));
        assertTrue(testMap.get("Infinite Loop").get("output").asText().startsWith("Test Failed!\njava.util.concurrent.TimeoutException"));
        assertTrue(testMap.get("Infinite Loop in Student Code").get("output").asText().startsWith("Test Failed!\njava.util.concurrent.TimeoutException"));
    }

    @Test
    public void assertFileCompilesWorksForJavaFX() {
        GradingUtils.setGraderConfigOptions(
            List.of(), false, false, 0, 0, 0,
            List.of(), List.of(), List.of(), true, false
        );
        assertEquals(
            List.of(), GradingUtils.getCompilerErrors(new File("src/test/java/grading1331/gradescope/JavaFXApp.java"))
        );
    }

}
