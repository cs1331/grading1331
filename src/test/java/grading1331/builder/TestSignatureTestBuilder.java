package grading1331.builder;

import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import grading1331.gradescope.GradedTestRunner;
import grading1331.gradescope.GradingUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class TestSignatureTestBuilder {
    @BeforeEach
    public void disableCheckstyle() {
        GradingUtils.setGraderConfigOptions(
            List.of(), false, false, 0, 0, 0,
            List.of(), List.of(), List.of(), false, false
        );
    }

    // test SignatureBuilder works for fields with all the different visibility modifiers
    @Test
    public void testVisibilityModifiersOnFields() {
        ObjectNode result = GradedTestRunner.runAllTests(
            new Class<?>[]{
                new GeneratedTestSuite() {
                    @Override
                    public List<RunnableTest> getGeneratedTests() {
                        return SignatureTestBuilder.createBuilder("grading1331.builder.ClassWithVisibilityModifierFields")
                            .withNonStaticSignatureTests(
                                new SignatureTestBuilder.SignatureWrapper() {
                                    private int thisShouldPass1;
                                    int thisShouldFail1;
                                    protected int thisShouldPass2;
                                    public int thisShouldFail2;
                                    int thisShouldPass3;
                                    private int thisShouldFail3;
                                    public int thisShouldPass4;
                                    protected int thisShouldFail4;
                                }
                            )
                            .allSeparateTests(2.0);
                    }
                }.getClass()
            }
        );
        ArrayNode tests = (ArrayNode) result.get("tests");
        assertEquals(10, tests.size());
        String[] visibilities = {"package-protected", "public", "private", "protected"};
        for (int i = 0; i < 4; i++) {
            ObjectNode passTest = (ObjectNode) tests.get(i * 2);
            assertEquals(2, passTest.get("score").asInt());
            ObjectNode failTest = (ObjectNode) tests.get(i * 2 + 1);
            assertEquals(0, failTest.get("score").asInt());
            String message = failTest.get("output").asText();
            assertTrue(
                message.startsWith(
                    "Test Failed!\norg.opentest4j.AssertionFailedError: Field thisShouldFail" + (i + 1)
                        + " in class ClassWithVisibilityModifierFields must be " + visibilities[i]
                ), message
            );
        }
    }

    @Test
    public void testFieldTypes() {
        ObjectNode result = GradedTestRunner.runAllTests(
            new Class<?>[]{
                new GeneratedTestSuite() {
                    @Override
                    public List<RunnableTest> getGeneratedTests() {
                        return SignatureTestBuilder.createBuilder("grading1331.builder.ClassWithTypeFields")
                            .withNonStaticSignatureTests(
                                new SignatureTestBuilder.SignatureWrapper() {
                                    private int thisShouldPass1;
                                    private int thisShouldFail1;
                                    private Object thisShouldPass2;
                                    private Object thisShouldFail2;
                                    private Object thisShouldPass3;
                                    private Object thisShouldFail3;
                                    private String thisShouldPass4;
                                    private String thisShouldFail4;
                                },
                                Map.of(
                                    "thisShouldPass2", "grading1331.builder.CustomClass1", "thisShouldFail2", "grading1331.builder.CustomClass1"
                                )
                            )
                            .allSeparateTests(2.0);
                    }
                }.getClass()
            }
        );
        ArrayNode tests = (ArrayNode) result.get("tests");
        assertEquals(10, tests.size());
        String[] types = {"int", "CustomClass1", "Object", "String"};
        for (int i = 0; i < 4; i++) {
            ObjectNode passTest = (ObjectNode) tests.get(i * 2);
            assertEquals(2, passTest.get("score").asInt());
            ObjectNode failTest = (ObjectNode) tests.get(i * 2 + 1);
            assertEquals(0, failTest.get("score").asInt());
            String message = failTest.get("output").asText();
            assertTrue(
                message.startsWith(
                    "Test Failed!\norg.opentest4j.AssertionFailedError: Your class should have a field named thisShouldFail" + (i + 1)
                        + " of type " + types[i]
                ), message
            );
        }
    }
}

class ClassWithVisibilityModifierFields {
    private int thisShouldPass1;
    private int thisShouldFail1;
    protected int thisShouldPass2;
    protected int thisShouldFail2;
    int thisShouldPass3;
    int thisShouldFail3;
    public int thisShouldPass4;
    public int thisShouldFail4;
}

class CustomClass1 {

}

class CustomClass2 {

}

class ClassWithTypeFields {
    private int thisShouldPass1;
    private short thisShouldFail1;
    private CustomClass1 thisShouldPass2;
    private Object thisShouldFail2;
    private Object thisShouldPass3;
    private CustomClass2 thisShouldFail3;
    private String thisShouldPass4;
    private Object thisShouldFail4;
}