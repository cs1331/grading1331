package grading1331;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

import grading1331.ReflectHelper.MethodInvocationReport;
import testutil.ExampleArbitraryClass;

public class TestReflectHelper {

    @Test
    public void testGetExistingClass() {
        ReflectHelper.getClass("testutil.ExampleArbitraryClass");
    }

    @Test
    public void testGetNonExistingClass() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.getClass("testutil.DoesntExist")
        );
    }

    @Test
    public void testGetDeclaredMethodExists1() {
        ReflectHelper.getDeclaredMethod(ExampleArbitraryClass.class, "timesByRandom", Random.class, Integer.class);
    }

    @Test
    public void testGetDeclaredMethodExists2() {
        ReflectHelper.getDeclaredMethod("testutil.ExampleArbitraryClass", "timesByRandom", Random.class, Integer.class);
    }

    @Test
    public void testGetDeclaredMethodNonPublic1() {
        ReflectHelper.getDeclaredMethod(ExampleArbitraryClass.class, "m");
    }

    @Test
    public void testGetDeclaredMethodNonPublic2() {
        ReflectHelper.getDeclaredMethod("testutil.ExampleArbitraryClass", "m");
    }

    @Test
    public void testGetDeclaredMethodNonExisting1() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.getDeclaredMethod(ExampleArbitraryClass.class, "lolwut")
        );
    }

    @Test
    public void testGetDeclaredMethodNonExisting2() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.getDeclaredMethod("testutil.ExampleArbitraryClass", "lolwut")
        );
    }

    @Test
    public void testGetDeclaredFieldExists1() {
        ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "zero");
    }

    @Test
    public void testGetDeclaredFieldExists2() {
        ReflectHelper.getDeclaredField("testutil.ExampleArbitraryClass", "zero");
    }

    @Test
    public void testGetDeclaredFieldNonPublic1() {
        ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "test");
    }

    @Test
    public void testGetDeclaredFieldNonPublic2() {
        ReflectHelper.getDeclaredField("testutil.ExampleArbitraryClass", "test");
    }

    @Test
    public void testGetDeclaredFieldNonExisting1() {
        assertThrows(AssertionError.class, 
            () -> ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "lolwut")
        );
    }

    @Test
    public void testGetDeclaredFieldNonExisting2() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.getDeclaredField("testutil.ExampleArbitraryClass", "lolwut")
        );
    }

    @Test
    public void testGetDeclaredConstructorExists1() {
        ReflectHelper.getDeclaredConstructor(ExampleArbitraryClass.class, String.class);
    }

    @Test
    public void testGetDeclaredConstructorExists2() {
        ReflectHelper.getDeclaredConstructor("testutil.ExampleArbitraryClass", String.class);
    }

    @Test
    public void testGetDeclaredConstructorNonPublic1() {
        ReflectHelper.getDeclaredConstructor(ExampleArbitraryClass.class);
    }

    @Test
    public void testGetDeclaredConstructorNonPublic2() {
        ReflectHelper.getDeclaredConstructor("testutil.ExampleArbitraryClass");
    }

    @Test
    public void testGetDeclaredConstructorNonExisting1() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.getDeclaredConstructor(ExampleArbitraryClass.class, Random.class)
        );
    }

    @Test
    public void testGetDeclaredConstructorNonExisting2() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.getDeclaredConstructor("testutil.ExampleArbitraryClass", Random.class)
        );
    }

    @Test
    public void testGetParamTypesFromParams() {
        Class<?>[] paramTypes = ReflectHelper.getParamTypesFromParams(1, "", new Random(), null);
        assertArrayEquals(new Class<?>[] {Integer.class, String.class, Random.class, Object.class}, paramTypes);
    }

    @Test
    public void testGetInstance1() {
        ReflectHelper.getInstance(ExampleArbitraryClass.class);
    }

    @Test
    public void testGetInstance2() {
        ReflectHelper.getInstance("testutil.ExampleArbitraryClass");
    }

    // todo add more tests for the new versions of getInstance

    @Test
    public void testGetInstanceNonPublic1() {
        ReflectHelper.getInstance(ExampleArbitraryClass.class);
    }

    @Test
    public void testGetInstanceNonPublic2() {
        ReflectHelper.getInstance("testutil.ExampleArbitraryClass");
    }

    @Test
    public void testInvokeMethodStaticPrint1() {
        MethodInvocationReport mir = ReflectHelper.invokeMethod(ExampleArbitraryClass.class, "print", null);
        assertEquals(String.format("Hello, world!!%n"), mir.sysoutCapture.toString());
        assertEquals("", mir.syserrCapture.toString());
        assertTrue(mir.exception.isEmpty());
        assertTrue(mir.returnValue.isEmpty());
    }

    @Test
    public void testInvokeMethodInstanceReturn1() {
        MethodInvocationReport mir = ReflectHelper.invokeMethod(ExampleArbitraryClass.class,
                "timesByRandom",
                ReflectHelper.getInstance(ExampleArbitraryClass.class),
                new Random(), 1);
        assertEquals("", mir.sysoutCapture.toString());
        assertEquals("", mir.syserrCapture.toString());
        assertTrue(mir.exception.isEmpty());
        assertTrue(mir.returnValue.isPresent());
        assertEquals(Integer.class, mir.returnValue.get().getClass());
    }

    @Test
    public void testInvokeMethodException1() {
        MethodInvocationReport mir = ReflectHelper.invokeMethod(ExampleArbitraryClass.class,
                "m",
                ReflectHelper.getInstance(ExampleArbitraryClass.class));
        assertEquals("", mir.sysoutCapture.toString());
        assertEquals("", mir.syserrCapture.toString());
        assertTrue(mir.exception.isPresent());
        assertTrue(mir.returnValue.isEmpty());
        assertEquals(NullPointerException.class, mir.exception.get().getClass());
    }

    @Test
    public void testInvokeMethodStaticPrint2() {
        MethodInvocationReport mir = ReflectHelper.invokeMethod(
                ReflectHelper.getDeclaredMethod(ExampleArbitraryClass.class, "print"),
                null);
        assertEquals(String.format("Hello, world!!%n"), mir.sysoutCapture.toString());
        assertEquals("", mir.syserrCapture.toString());
        assertTrue(mir.exception.isEmpty());
        assertTrue(mir.returnValue.isEmpty());
    }

    @Test
    public void testInvokeMethodInstanceReturn2() {
        MethodInvocationReport mir = ReflectHelper.invokeMethod(
                ReflectHelper.getDeclaredMethod(ExampleArbitraryClass.class, "timesByRandom", Random.class, Integer.class),
                ReflectHelper.getInstance(ExampleArbitraryClass.class),
                new Random(), 1);
        assertEquals("", mir.sysoutCapture.toString());
        assertEquals("", mir.syserrCapture.toString());
        assertTrue(mir.exception.isEmpty());
        assertTrue(mir.returnValue.isPresent());
        assertEquals(Integer.class, mir.returnValue.get().getClass());
    }

    @Test
    public void testInvokeMethodException2() {
        MethodInvocationReport mir = ReflectHelper.invokeMethod(
                ReflectHelper.getDeclaredMethod(ExampleArbitraryClass.class, "m"),
                ReflectHelper.getInstance(ExampleArbitraryClass.class));
        assertEquals("", mir.sysoutCapture.toString());
        assertEquals("", mir.syserrCapture.toString());
        assertTrue(mir.exception.isPresent());
        assertTrue(mir.returnValue.isEmpty());
        assertEquals(NullPointerException.class, mir.exception.get().getClass());
    }

    @Test
    public void testSetFieldStatic1() {
        ReflectHelper.setField(ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "a"), null, null);
    }

    @Test
    public void testSetFieldInstance1() {
        ReflectHelper.setField(ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "settable"),
                ReflectHelper.getInstance(ExampleArbitraryClass.class), 0);
    }

    @Test
    public void testSetFieldNonPublicStatic1() {
        ReflectHelper.setField(ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "b"), null, null);
    }

    @Test
    public void testSetFieldNonPublicInstance1() {
        ReflectHelper.setField(ReflectHelper.getDeclaredField(ExampleArbitraryClass.class, "test"),
                ReflectHelper.getInstance(ExampleArbitraryClass.class), 0);
    }

    @Test
    public void testSetFieldStatic2() {
        ReflectHelper.setField("a", ExampleArbitraryClass.class, null, null);
    }

    @Test
    public void testSetFieldInstance2() {
        ReflectHelper.setField("settable", ExampleArbitraryClass.class,
                ReflectHelper.getInstance(ExampleArbitraryClass.class), 0);
    }

    @Test
    public void testSetFieldNonPublicStatic2() {
        ReflectHelper.setField("b", ExampleArbitraryClass.class, null, null);
    }

    @Test
    public void testSetFieldNonPublicInstance2() {
        ReflectHelper.setField("test", ExampleArbitraryClass.class,
                ReflectHelper.getInstance(ExampleArbitraryClass.class), 0);
    }

    @Test
    public void testSetFieldNonExisting2() {
        assertThrows(AssertionError.class,
            () -> ReflectHelper.setField("lolwut", ExampleArbitraryClass.class, null, null)
        );
    }

    private static class NormalClass {
        private int field1;
        private String field2;

        public NormalClass(int a, String b) {
            this.field1 = a;
            this.field2 = b;
        }

        public int getField1() {
            return field1;
        }

        public void setField1(int newField1) {
            this.field1 = newField1;
        }

        public String getField2() {
            return field2;
        }

        public void setField2(String newField2) {
            this.field2 = newField2;
        }
    }

    private static class BadClass {

        private int field1;
        private String field2;

        public BadClass(int a, String b) {
            this.field1 = a;
            this.field2 = b;
        }

        public String getField2() {
            return field2 == null ? null : field2.substring(0, 1);
        }

        public void setField1(int newField1) {
            field1 = ++newField1;
        }

        public void setField2(String newField2) {
            field2 = null;
        }
    }

    private static class ClassWithBadEquals {
        public boolean equals(Object other) {return false;}
    }

    private static class ClassUsingBadClass {
        private int field1;
        private ClassWithBadEquals field2;

        public ClassUsingBadClass(int a, ClassWithBadEquals b) {
            this.field1 = a;
            this.field2 = b;
        }

        public int getField1() {
            return field1;
        }

        public void setField1(int newField1) {
            this.field1 = newField1;
        }

        public ClassWithBadEquals getField2() {
            return field2;
        }

        public void setField2(ClassWithBadEquals newField2) {
            this.field2 = newField2;
        }

        public boolean equals(Object other) {
            return false;
        }
    }

    @Test
    public void testCheckGetterNormalClass() {
        Object instance = new NormalClass(1, "hi");
        ReflectHelper.checkGetter(instance, "field1", 1);
        ReflectHelper.checkGetter(instance, "field2", "hi");
    }

    @Test
    public void testCheckSetterNormalClass() {
        Object instance = new NormalClass(1, "hi");
        ReflectHelper.checkSetter(instance, "field1", int.class, 4, 5);
        ReflectHelper.checkSetter(instance, "field2", String.class, "hello", "bye");
    }

    @Test
    public void testCheckGetterBadClass() {
        Object instance = new BadClass(1, "hi");
        assertThrows(AssertionError.class, () -> ReflectHelper.checkGetter(instance, "field1", 1));
        assertThrows(AssertionError.class, () ->ReflectHelper.checkGetter(instance, "field2", "hi"));
    }

    @Test
    public void testCheckSetterBadClass() {
        Object instance = new BadClass(1, "hi");
        assertThrows(AssertionError.class, () -> ReflectHelper.checkSetter(instance, "field1", int.class, 4, 5));
        assertThrows(AssertionError.class, () -> ReflectHelper.checkSetter(instance, "field2", String.class, "hello", "bye"));
    }

    @Test
    public void testCheckGetterBadEquals() {
        ClassWithBadEquals obj = new ClassWithBadEquals();
        Object instance = new ClassUsingBadClass(1, obj);
        ReflectHelper.checkGetter(instance, "field1", 1);
        ReflectHelper.checkGetter(instance, "field2", obj);
    }

    @Test
    public void testCheckSetterBadEquals() {
        ClassWithBadEquals obj = new ClassWithBadEquals();
        ClassWithBadEquals obj2 = new ClassWithBadEquals();
        Object instance = new ClassUsingBadClass(1, obj);
        ReflectHelper.checkSetter(instance, "field1", int.class, 4, 5);
        ReflectHelper.checkSetter(instance, "field2", ClassWithBadEquals.class, obj, obj2);
    }

    /**
     * Credit to Emma Barron (ebarron6) for the 9 tests below. I pulled & adapted them from her development branch
     * on a much older version of the grading1331 repo.
     */
    @Test
    public void testCheckGetterGetterNonExisting() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkGetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "settable", 91)
        );
    }

    @Test
    public void testCheckGetterFieldNonExisting() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkGetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "i_dont_exist", 12.4)
        );
    }

    @Test
    public void testCheckGetterIncorrect() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkGetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "wrongGetterSetter", "string-string-string")
        );
    }

    @Test
    public void testCheckGetterCorrect() {
        ReflectHelper.checkGetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "getterSetterCheck", "getterCheck value!");
    }

    @Test
    public void testSetterWrongReturnType() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkSetter(ReflectHelper.getInstance(ExampleArbitraryClass.class),"test", Integer.TYPE, 12, 34)
        );
    }

    @Test
    public void testSetterSetterNonExisting() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkSetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "settable", Integer.TYPE, 12, 34)
        );
    }

    @Test
    public void testSetterFieldNonExisting() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkSetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "i_dont_exist", Integer.TYPE, 12, 34)
        );
    }

    @Test
    public void testSetterDoesNotSet() {
        assertThrows(AssertionError.class, () ->
            ReflectHelper.checkSetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "wrongGetterSetter", String.class, "startValue", "setValue")
        );
    }

    @Test
    public void testSetterCorrect() {
        ReflectHelper.checkSetter(ReflectHelper.getInstance(ExampleArbitraryClass.class), "getterSetterCheck", String.class, "startValue", "setValue");
    }

    // todo add tests for ReflectHelper methods that call forceDefaultConstructor and assign fields

    private static abstract class A {
        private final String a;

        public A(String a) {
            this.a = a;
        }

        public String toString() {
            return "I am named " + a;
        }
    }

    private interface B {
        String hello(String ok);

        default String ok() {
            return "ok";
        }
    }

    @Test
    public void testGetAbstractClassInstance() {
        Object instance = ReflectHelper.getAbstractClassInstance(A.class, Map.of(), new Class<?>[]{String.class}, "Bob");
        assertEquals("Bob", ReflectHelper.getFieldValue("a", A.class, instance));
        Method toStringMethod = ReflectHelper.getDeclaredMethod(A.class, "toString");
        MethodInvocationReport toStringInvocation = ReflectHelper.invokeMethod(toStringMethod, instance);
        assertEquals("I am named Bob", toStringInvocation.returnValue.get());
    }

    // TODO add tests where we actually care about abstract method functionality in abstract class

    @Test
    public void testGetInterfaceInstance() {
        Method helloMethod = ReflectHelper.getDeclaredMethod(B.class, "hello", String.class);
        HashMap<Method, Function<Object[], Object>> methodFunctionMap = new HashMap<>();
        methodFunctionMap.put(helloMethod, paramList -> paramList[0] + " yeet");
        Object interfaceInstance = ReflectHelper.getInterfaceInstance(B.class, methodFunctionMap);
        MethodInvocationReport report = ReflectHelper.invokeMethod(helloMethod, interfaceInstance, "ok");
        assertEquals("ok yeet", report.returnValue.get());
    }

}
