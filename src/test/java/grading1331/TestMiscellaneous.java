package grading1331;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Random;

public class TestMiscellaneous {

    @Test
    public void testStringDistance() {
        // none of these should fail - all are true
        assertEquals(Miscellaneous.stringDistance("hello", "hello"), 0);
        assertEquals(Miscellaneous.stringDistance("Hi, my name is cs1331", "Hi, my name is cs1331"), 0);
        assertEquals(Miscellaneous.stringDistance("!@#$%^&*()", "!@#$%^&*()"), 0);
        assertEquals(Miscellaneous.stringDistance("", ""), 0);
        assertEquals(Miscellaneous.stringDistance("yellow", "fellow"), 1);
        assertEquals(Miscellaneous.stringDistance("hello", "yellow"), 2);
        assertEquals(Miscellaneous.stringDistance("kitten", "sitting"), 3);
        assertEquals(Miscellaneous.stringDistance("sitting", "kitten"), 3);
        assertTrue(Miscellaneous.stringDistance("y@#tt", "y* re") <= 5);
    }

    @Test
    public void testStringDistanceFirstParamNull() {
        assertThrows(AssertionError.class,
            () -> assertEquals(Miscellaneous.stringDistance(null, "hello"), 3)
        );
    }

    @Test
    public void testStringDistanceSecondParamNull() {
        assertThrows(AssertionError.class,
            () -> assertEquals(Miscellaneous.stringDistance("hello", null), 4)
        );
    }

    @Test
    public void testStringDistanceDistanceNegative() {
        assertThrows(AssertionError.class,
            () -> assertEquals(Miscellaneous.stringDistance("hello", "hello"), -3)
        );
    }

    @Test
    public void testStringDistanceDistanceZero() {
        assertThrows(AssertionError.class,
            () -> assertEquals(Miscellaneous.stringDistance("hello", "yellow"), 0)
        );
    }

    @Test
    public void testStringDistanceValueIsThree() {
        assertThrows(AssertionError.class,
            () -> assertEquals(Miscellaneous.stringDistance("sitting", "kitten"), 2)
        );
    }

    @Test
    public void testMarkFirstDistance() {
        assertEquals(Miscellaneous.markFirstDifference("asdfasdf", "asdf"), "    ^");
    }

    @Test
    public void testStringDistanceAssert() {
        assertThrows(AssertionError.class,
            () -> Miscellaneous.assertStringsMatch("abc", "123", 2, false)
        );
        Miscellaneous.assertStringsMatch("abc", "123", 3, false);
        Miscellaneous.assertStringsMatch("abc", "abc", 0, false);
    }

    @Test
    public void testRandomCustomSubclass() throws IllegalAccessException {
        Miscellaneous.setMathDotRandom(
            new Random() {
                public double nextDouble() {
                    return -2;
                }
            }
        );
        assertEquals(Math.random(), -2, .00001);
    }

    @Test
    public void testRandomSuppliedFunction() throws IllegalAccessException {
        Miscellaneous.setMathDotRandom(i -> 0.05 + 0.1 * (i % 10));
        for (int i = 0; i < 12; i++) {
            assertEquals(Math.random(), 0.05 + 0.1 * (i % 10), .00001);
        }
    }
}
